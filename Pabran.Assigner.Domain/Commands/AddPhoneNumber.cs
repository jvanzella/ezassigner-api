﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AddPhoneNumber : UserMessage, ICommand
   {
      public PhoneNumber PhoneNumber { get; }

      public AddPhoneNumber(UserId id, PhoneNumber phoneNumber) 
         : base(id)
      {
         PhoneNumber = phoneNumber;
      }
   }
}