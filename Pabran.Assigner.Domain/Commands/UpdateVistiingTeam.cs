﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateVisitingTeam : GameMessage, ICommand
   {
      public Team Team { get; }

      public UpdateVisitingTeam(LeagueId leagueId, SeasonId seasonid, GameNumber gameNumber, Team team)
         : base(leagueId.Value(), seasonid.Value(), gameNumber.Value())
      {
         Team = team.Value();
      }
   }
}