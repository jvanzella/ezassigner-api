﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class CreateSeason : LeagueMessage, ICommand
   {
      public SeasonId SeasonId { get; set; }

      public CreateSeason(LeagueId leagueId, SeasonId seasonId)
         : base(leagueId.Value())
      {
         SeasonId = seasonId.Value();
      }
   }
}