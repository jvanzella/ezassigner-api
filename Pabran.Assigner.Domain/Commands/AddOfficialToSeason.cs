﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AddOfficialToSeason : SeasonMessage, ICommand
   {
      public UserId UserId { get; private set; }
      public string Rating { get; private set; }

      public AddOfficialToSeason(LeagueId leagueId, SeasonId seasonId, UserId userId, string rating) 
         : base(leagueId.Value(), seasonId.Value())
      {
         Rating = rating.Value();
         UserId = userId.Value();
      }
   }
}