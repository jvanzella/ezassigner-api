﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class CreateLeague : LeagueMessage, ICommand
   {
      public CreateLeague(LeagueId id) 
         : base(id.Value())
      {
      }
   }
}