﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportFacilityDetails : FacilityMessage, ICommand
   {
      public ReportFacilityDetails(FacilityId id) 
         : base(id.Value())
      {
      }
   }
}