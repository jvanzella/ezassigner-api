﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AddDivisionToSeason : SeasonMessage, ICommand
   {
      public Division Division { get; private set; } 

      public AddDivisionToSeason(LeagueId leagueId, SeasonId seasonId, Division division) 
         : base(leagueId.Value(), seasonId.Value())
      {
         Division = division.Value();
      }
   }
}