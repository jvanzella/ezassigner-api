﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportSeasonDetails : SeasonMessage, ICommand
   {
      public ReportSeasonDetails(LeagueId leagueId, SeasonId seasonId) 
         : base(leagueId.Value(), seasonId.Value())
      {
      }
   }
}