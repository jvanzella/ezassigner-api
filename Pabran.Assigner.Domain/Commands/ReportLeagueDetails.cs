﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportLeagueDetails : LeagueMessage, ICommand
   {
      public ReportLeagueDetails(LeagueId leagueId) 
         : base(leagueId.Value())
      {
      }
   }
}