﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportRatingDetails : SeasonMessage, ICommand
   {
      public string RatingId { get; }

      public ReportRatingDetails(LeagueId leagueId, SeasonId seasonId, string ratingId) 
         : base(leagueId.Value(), seasonId.Value())
      {
         RatingId = ratingId.Value();
      }
   }
}