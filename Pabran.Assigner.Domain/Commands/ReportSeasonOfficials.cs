﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportSeasonOfficials : SeasonMessage, ICommand
   {
      public ReportSeasonOfficials(LeagueId leagueId, SeasonId seasonId) 
         : base(leagueId.Value(), seasonId.Value())
      {
      }
   }
}