﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateGameTime : GameMessage, ICommand
   {
      public GameTime GameTime { get; }

      public UpdateGameTime(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber, GameTime gameTime) :
         base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
         GameTime = gameTime.Value();
      }
   }
}