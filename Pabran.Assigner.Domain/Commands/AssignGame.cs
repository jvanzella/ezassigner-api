﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AssignGame : GameMessage, ICommand
   {
      public UserId UserId { get; private set; }
      public string Position { get; private set; }

      public AssignGame(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber, UserId userId, string position)
         : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
         Position = position.Value();
         UserId = userId.Value();
      }
   }
}