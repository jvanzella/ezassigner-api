﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateLeagueName : LeagueMessage, ICommand
   {
      public string Name { get; private set; }
      
      public UpdateLeagueName(LeagueId leagueId, string name)
         : base(leagueId.Value())
      {
         Name = name.Value();
      }
   }
}