﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateFacilityName : FacilityMessage, ICommand
   {
      public string Name { get; private set; }

      public UpdateFacilityName(FacilityId id, string name) 
         : base(id.Value())
      {
         Name = name.Value();
      }
   }
}