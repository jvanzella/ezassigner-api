﻿// /////////////////////////////////////////////////////////
//  EmailAddressUpdated.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateEmailAddress : UserMessage, ICommand
   {
      public EmailAddress EmailAddress { get; }

      public UpdateEmailAddress(UserId id, EmailAddress emailAddress) : base(id)
      {
         EmailAddress = emailAddress;
      }
   }
}