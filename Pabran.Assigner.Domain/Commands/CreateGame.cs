﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class CreateGame : SeasonMessage, ICommand
   {
      public GameNumber GameNumber { get; }

      public CreateGame(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber) 
         : base(leagueId.Value(), seasonId.Value())
      {
         GameNumber = gameNumber.Value();
      }
   }
}