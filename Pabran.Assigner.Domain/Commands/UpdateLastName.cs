﻿// /////////////////////////////////////////////////////////
//  UpdateLastName.cs
// 
//  Created on:      11/22/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateLastName : UserMessage, ICommand
   {
      public string LastName { get; }

      public UpdateLastName(UserId id, string lastName) 
         : base(id.Value())
      {
         LastName = lastName.Value();
      }
   }
}