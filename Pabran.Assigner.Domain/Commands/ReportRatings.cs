﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportRatings : SeasonMessage, ICommand
   {
      public ReportRatings(LeagueId leagueId, SeasonId seasonId) 
         : base(leagueId.Value(), seasonId.Value())
      {
      }
   }
}