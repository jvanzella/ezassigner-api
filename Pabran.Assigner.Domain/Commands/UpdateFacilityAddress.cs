﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateFacilityAddress : FacilityMessage, ICommand
   {
      public StreetAddress Address { get; private set; }

      public UpdateFacilityAddress(FacilityId id, StreetAddress address) 
         : base(id.Value())
      {
         Address = address.Value();
      }
   }
}