﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class CreateFacility : FacilityMessage, ICommand
   {
      public CreateFacility(FacilityId id) 
         : base(id.Value())
      {
      }
   }
}