﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportAssignments : GameMessage, ICommand
   {
      public ReportAssignments(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber) 
         : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
      }
   }
}