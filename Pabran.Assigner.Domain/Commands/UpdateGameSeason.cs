﻿using System;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class UpdateGameSeason : GameMessage, ICommand
   {
      public SeasonId TargetSeasonId { get; }

      public UpdateGameSeason(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber, SeasonId targetSeasonId) 
         : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
         if (targetSeasonId == null) throw new ArgumentNullException(nameof(targetSeasonId));

         TargetSeasonId = targetSeasonId.Value();
      }
   }
}