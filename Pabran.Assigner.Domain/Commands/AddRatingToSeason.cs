﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AddRatingToSeason : SeasonMessage, ICommand
   {
      public Rating Rating { get; private set; } 

      public AddRatingToSeason(LeagueId leagueId, SeasonId seasonId, Rating rating)
         : base(leagueId.Value(), seasonId.Value())
      {
         Rating = rating.Value();
      }
   }
}