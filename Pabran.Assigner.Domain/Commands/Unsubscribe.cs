﻿// /////////////////////////////////////////////////////////
//  Unsubscribe.cs
// 
//  Created on:      11/12/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;
using Akka.Actor;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class Unsubscribe
   {
      public IActorRef Actor { get; private set; }
      public Type Channel { get; private set; }

      public Unsubscribe(IActorRef actor, Type channel)
      {
         Actor = actor;
         Channel = channel;
      }
   }
}