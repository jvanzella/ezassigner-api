﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportGameDetails : GameMessage, ICommand
   {
      public ReportGameDetails(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber) 
         : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
      }
   }
}