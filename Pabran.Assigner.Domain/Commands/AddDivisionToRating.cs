﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class AddDivisionToRating : SeasonMessage, ICommand
   {
      public string DivisionId { get; private set; }
      public string Rating { get; private set; }

      public AddDivisionToRating(LeagueId leagueId, SeasonId seasonId, string divisionId, string rating) 
         : base(leagueId.Value(), seasonId.Value())
      {
         DivisionId = divisionId.Value();
         Rating = rating.Value();
      }
   }
}