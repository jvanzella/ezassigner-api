﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportUserDetails : UserMessage, ICommand
   {
      public ReportUserDetails(UserId id) 
         : base(id.Value())
      {
      }
   }
}