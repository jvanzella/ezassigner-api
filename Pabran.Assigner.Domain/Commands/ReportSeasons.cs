﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Commands
{
   public sealed class ReportSeasons : LeagueMessage, ICommand
   {
      public ReportSeasons(LeagueId leagueId) 
         : base(leagueId.Value())
      {
      }
   }
}