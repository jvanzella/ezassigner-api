﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class FacilityNameUpdated : FacilityMessage, IDomainEvent
   {
      public string Name { get; private set; }

      public FacilityNameUpdated(FacilityId id, string name) : base(id.Value())
      {
         Name = name.Value();
      }
   }
}