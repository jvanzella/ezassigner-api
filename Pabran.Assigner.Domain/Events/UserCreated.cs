﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class UserCreated : UserMessage, IDomainEvent
   {
      public UserCreated(UserId id) : base(id)
      {
      }
   }
}