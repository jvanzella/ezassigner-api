﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class HomeTeamUpdated : GameMessage, IDomainEvent
   {
      public Team Team { get; }

      public HomeTeamUpdated(LeagueId leagueId, SeasonId seasonid, GameNumber gameNumber, Team team)
         : base(leagueId.Value(), seasonid.Value(), gameNumber.Value())
      {
         Team = team;
      } 
   }
}