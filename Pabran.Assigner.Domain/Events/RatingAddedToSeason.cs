﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class RatingAddedToSeason : SeasonMessage, IDomainEvent
   { 
      public Rating Rating { get; }

      public RatingAddedToSeason(LeagueId leagueId, SeasonId seasonId, Rating rating) 
         : base(leagueId.Value(), seasonId.Value())
      {
         Rating = rating;
      }
   }
}