﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class SeasonCreated : LeagueMessage, IDomainEvent
   {
      public SeasonId SeasonId { get; set; }

      public SeasonCreated(LeagueId leagueId, SeasonId seasonId)
         : base(leagueId)
      {
         SeasonId = seasonId.Value();
      }
   }
}