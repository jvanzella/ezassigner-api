﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class OfficialAddedToSeason : SeasonMessage, IDomainEvent
   {
      public UserId UserId { get; private set; }
      public string Rating { get; private set; }

      public OfficialAddedToSeason(LeagueId leagueId, SeasonId seasonId, UserId userId, string rating)
         : base(leagueId.Value(), seasonId.Value())
      {
         Rating = rating.Value();
         UserId = userId;
      }
   }
}