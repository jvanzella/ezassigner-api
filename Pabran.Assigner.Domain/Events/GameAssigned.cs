﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class GameAssigned : GameMessage, IDomainEvent
   {
      public UserId UserId { get; private set; }
      public string Position { get; private set; }

      public GameAssigned(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber, UserId userId, string position)
         : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
         Position = position;
         UserId = userId;
      }
   }
}