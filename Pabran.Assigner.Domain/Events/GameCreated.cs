﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class GameCreated : SeasonMessage, IDomainEvent
   {
      public GameNumber GameNumber { get; }

      public GameCreated(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber) : base(leagueId.Value(), seasonId.Value())
      {
         GameNumber = gameNumber.Value();
      }
   }
}