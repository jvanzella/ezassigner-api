﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class DivisionAddedToSeason : SeasonMessage, IDomainEvent
   {
      public Division Division { get; private set; }

      public DivisionAddedToSeason(LeagueId leagueId, SeasonId seasonId, Division division)
         : base(leagueId.Value(), seasonId.Value())
      {
         Division = division;
      }
   }
}