﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class FacilityAddressUpdated : FacilityMessage, IDomainEvent
   {
      public StreetAddress Address { get; private set; }

      public FacilityAddressUpdated(FacilityId id, StreetAddress address) : base(id.Value())
      {
         Address = address.Value();
      }
   }
}