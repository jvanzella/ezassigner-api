﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class LeagueCreated : LeagueMessage, IDomainEvent
   {
      public LeagueCreated(LeagueId id) : base(id)
      {
      }
   }
}