﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class DivisionAddedToRating : SeasonMessage, IDomainEvent
   {
      public string DivisionId { get; private set; }
      public string Rating { get; private set; }

      public DivisionAddedToRating(LeagueId leagueId, SeasonId seasonId, string divisionId, string rating)
         : base(leagueId.Value(), seasonId.Value())
      {
         DivisionId = divisionId.Value();
         Rating = rating.Value();
      }
   }
}