﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class GameTimeUpdated : GameMessage, IDomainEvent
   {
      public GameTime GameTime { get; }

      public GameTimeUpdated(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber, GameTime gameTime) : base(leagueId.Value(), seasonId.Value(), gameNumber.Value())
      {
         GameTime = gameTime.Value();
      } 
   }
}