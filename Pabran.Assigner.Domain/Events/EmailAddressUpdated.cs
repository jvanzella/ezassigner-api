﻿// /////////////////////////////////////////////////////////
//  EmailAddressUpdated.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public sealed class EmailAddressUpdated : UserMessage, IDomainEvent
   {
      public EmailAddress EmailAddress { get; }

      public EmailAddressUpdated(UserId id, EmailAddress emailAddress) : base(id)
      {
         EmailAddress = emailAddress;
      }
   }
}