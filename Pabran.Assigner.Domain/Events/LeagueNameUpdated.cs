﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class LeagueNameUpdated : LeagueMessage, IDomainEvent
   {
      public string Name { get; private set; }

      public LeagueNameUpdated(LeagueId leagueId, string name)
         : base(leagueId)
      {
         Name = name;
      } 
   }
}