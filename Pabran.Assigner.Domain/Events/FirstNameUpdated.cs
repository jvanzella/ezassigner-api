﻿// /////////////////////////////////////////////////////////
//  FirstNameUpdated.cs
// 
//  Created on:      11/20/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Events
{
   public class FirstNameUpdated : UserMessage, IDomainEvent
   {
      public string FirstName { get; }

      public FirstNameUpdated(UserId id, string firstName) 
         : base(id.Value())
      {
         FirstName = firstName.Value();
      }
   }
}