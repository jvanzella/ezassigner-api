﻿using Akka.Actor;

namespace Pabran.Assigner.Domain.Infrastructure.Domain
{
   public abstract class Entity<TState> : ReceiveActor
   {
      protected TState State { get; set; }
   }
}