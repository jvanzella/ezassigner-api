﻿using System;
using Akka.Event;
using Akka.Persistence;

namespace Pabran.Assigner.Domain.Infrastructure.Domain
{
   public abstract class AggregateRoot<TState> : ReceivePersistentActor
   {
      protected readonly ILoggingAdapter Logger = Context.GetLogger();

      private int _eventCount;

      protected TState State { get; set; }

      public override string PersistenceId { get; }

      protected AggregateRoot()
      {
         var path = Self.Path;
         PersistenceId = path.Parent.Name + "/" + path.Name;
      }

      protected abstract bool UpdateState(IDomainEvent domainEvent);

      protected void Emit<TEvent>(TEvent domainEvent, Action<TEvent> handler = null) where TEvent : IDomainEvent
      {
         Persist(domainEvent, e =>
         {
            Logger.Info($"Processing event [{domainEvent.GetType().Name}].");

            if (!UpdateState(e))
            {
               return;
            }

            SaveSnapshotIfNecessary();
            handler?.Invoke(domainEvent);

            Context.System.EventStream.Publish(domainEvent);
         });
      }

      private void SaveSnapshotIfNecessary()
      {
         _eventCount = ++_eventCount % 200;
         if (_eventCount == 0)
         {
            SaveSnapshot(State);
         }
      }
   }
}