﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Core.Extensions;

namespace Pabran.Assigner.Domain.Infrastructure.Extensions
{
   public static class GameTimeExtensions
   {
      public static GameTimeDto ToGameTimeDto(this GameTime time)
      {
         return new GameTimeDto
         {
            StartTime = time.StartTime.ToSimpleDateTimeDto(),
            EndTime = time.EndTime.ToSimpleDateTimeDto()
         };
      }

      public static GameTime ToGameTime(this GameTimeDto time)
      {
         return new GameTime(time.StartTime.ToDateTimeOffset(), time.EndTime.ToDateTimeOffset());
      }
   }
}