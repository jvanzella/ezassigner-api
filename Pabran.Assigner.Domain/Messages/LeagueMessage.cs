using System;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Messages
{
   public abstract class LeagueMessage
   {
      public LeagueId LeagueId { get; }
      
      protected LeagueMessage(LeagueId id)
      {
         if (id == null) throw new ArgumentNullException(nameof(id));

         LeagueId = id.Value();
      }
   }
}