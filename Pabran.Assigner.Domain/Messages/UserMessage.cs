﻿using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Messages
{
   public class UserMessage
   {
      public UserId UserId { get; private set; }

      public UserMessage(UserId id)
      {
         UserId = id.Value();
      }
   }
}