﻿using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Messages
{
   public abstract class FacilityMessage
   {
      public FacilityId Id { get; private set; }

      protected FacilityMessage(FacilityId id)
      {
         Id = id.Value();
      }
   }
}