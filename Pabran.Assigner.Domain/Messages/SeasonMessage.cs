﻿using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Messages
{
   public class SeasonMessage
   {
      public SeasonId SeasonId { get; }
      public LeagueId LeagueId { get; }

      public SeasonMessage(LeagueId leagueId, SeasonId seasonId)
      {
         LeagueId = leagueId.Value();
         SeasonId = seasonId.Value();
      }
   }
}