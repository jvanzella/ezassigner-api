using System;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Messages
{
   public abstract class GameMessage
   {
      public LeagueId LeagueId { get; }
      public SeasonId SeasonId { get; }
      public GameNumber GameNumber { get; }

      protected GameMessage(LeagueId leagueId, SeasonId seasonId, GameNumber gameNumber)
      {
         if (leagueId == null) throw new ArgumentNullException(nameof(leagueId));
         if (seasonId == null) throw new ArgumentNullException(nameof(seasonId));
         if (gameNumber == null) throw new ArgumentNullException(nameof(gameNumber));

         LeagueId = leagueId.Value();
         SeasonId = seasonId.Value();
         GameNumber = gameNumber.Value();
      }
   }
}