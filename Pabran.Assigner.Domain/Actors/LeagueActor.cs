﻿using System.Linq;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public sealed partial class LeagueActor : AggregateRoot<LeagueModel>
   {
      public LeagueActor()
      {
         // Commands
         Command<ReportGameDetails>(Handle);
         Command<ReportLeagueDetails>(Handle);
         Command<ReportRatings>(Handle);
         Command<ReportDivisions>(Handle);
         Command<ReportSeasonOfficials>(Handle);
         Command<ReportSeasons>(Handle);
         Command<ReportRatingDetails>(Handle);

         // League
         Command<UpdateLeagueName>(updateLeagueName => Emit(new LeagueNameUpdated(updateLeagueName.LeagueId.Value(), updateLeagueName.Name.Value())));
         Command<CreateSeason>(addSeasonToLeague => Emit(new SeasonCreated(addSeasonToLeague.LeagueId.Value(), addSeasonToLeague.SeasonId.Value())));

         // Season
         Command<CreateGame>(createGame => Emit(new GameCreated(createGame.LeagueId.Value(), createGame.SeasonId.Value(), createGame.GameNumber.Value())));
         Command<CreateLeague>(createLeague => Emit(new LeagueCreated(createLeague.LeagueId.Value())));
         Command<AddOfficialToSeason>(addOfficial => Emit(new OfficialAddedToSeason(addOfficial.LeagueId.Value(), addOfficial.SeasonId.Value(), addOfficial.UserId.Value(), addOfficial.Rating.Value())));
         Command<AddDivisionToSeason>(addDivision => Emit(new DivisionAddedToSeason(addDivision.LeagueId.Value(), addDivision.SeasonId.Value(), addDivision.Division.Value())));
         Command<AddRatingToSeason>(addRating => Emit(new RatingAddedToSeason(addRating.LeagueId.Value(), addRating.SeasonId.Value(), addRating.Rating.Value())));
         Command<AddDivisionToRating>(addDivision => Emit(new DivisionAddedToRating(addDivision.LeagueId.Value(), addDivision.SeasonId.Value(), addDivision.DivisionId.Value(), addDivision.Rating.Value())));
         
         // Game
         Command<AssignGame>(assignGame => Emit(new GameAssigned(assignGame.LeagueId.Value(), assignGame.SeasonId.Value(), assignGame.GameNumber.Value(), assignGame.UserId.Value(), assignGame.Position.Value())));
         Command<UpdateGameTime>(updateGameTime => Emit(new GameTimeUpdated(updateGameTime.LeagueId.Value(), updateGameTime.SeasonId.Value(), updateGameTime.GameNumber.Value(), updateGameTime.GameTime.Value())));
         Command<UpdateGameSeason>(updateGameSeason => Emit(new GameSeasonUpdated(updateGameSeason.LeagueId.Value(), updateGameSeason.SeasonId.Value(), updateGameSeason.GameNumber.Value(), updateGameSeason.TargetSeasonId.Value())));
         Command<UpdateHomeTeam>(updateHomeTeam => Emit(new HomeTeamUpdated(updateHomeTeam.LeagueId.Value(), updateHomeTeam.SeasonId.Value(), updateHomeTeam.GameNumber.Value(), updateHomeTeam.Team.Value())));
         Command<UpdateVisitingTeam>(updateHomeTeam => Emit(new VisitingTeamUpdated(updateHomeTeam.LeagueId.Value(), updateHomeTeam.SeasonId.Value(), updateHomeTeam.GameNumber.Value(), updateHomeTeam.Team.Value())));
      }

      private bool Handle(ReportSeasons reportSeasons)
      {
         Logger.Info("Processing command [ReportSeasons].");

         var seasons = new SeasonsDto
         {
            Seasons = State.Seasons.Select(s => s.Id).ToArray()
         };
         
         Sender.Tell(seasons, Self);

         return true;
      }

      private bool Handle(ReportLeagueDetails reportLeagueDetails)
      {
         Logger.Info("Processing command [ReportLeagueDetails].");

         var dto = new LeagueDto
            {
               Id = State.Id.Id,
               Name = State.Name.Value(),
               Seasons= State.Seasons.Select(s => s.Id).ToArray()
            };

         Sender.Tell(dto, Self);

         return true;
      }

      protected override bool UpdateState(IDomainEvent domainEvent)
      {
         return Handle((dynamic) domainEvent);
      }
   }
}