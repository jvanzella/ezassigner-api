﻿using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Messages;

namespace Pabran.Assigner.Domain.Actors
{
   public class LeagueManagerActor : ReceiveActor
   {
      public LeagueManagerActor()
      {
         ReceiveAsync<CreateLeague>(CreateLeagueAsync);
         ReceiveAsync<UpdateLeagueName>(ForwardMessageAsync);
         ReceiveAsync<ReportLeagueDetails>(ForwardMessageAsync);
         ReceiveAsync<CreateGame>(ForwardMessageAsync);
         ReceiveAsync<ReportGameDetails>(ForwardMessageAsync);
         ReceiveAsync<UpdateGameTime>(ForwardMessageAsync);
         ReceiveAsync<CreateSeason>(ForwardMessageAsync);
         ReceiveAsync<ReportSeasons>(ForwardMessageAsync);
         ReceiveAsync<UpdateGameSeason>(ForwardMessageAsync);
         ReceiveAsync<ReportDivisions>(ForwardMessageAsync);
         ReceiveAsync<AddDivisionToSeason>(ForwardMessageAsync);
         ReceiveAsync<AddRatingToSeason>(ForwardMessageAsync);
         ReceiveAsync<ReportRatings>(ForwardMessageAsync);
         ReceiveAsync<AddDivisionToRating>(ForwardMessageAsync);
         ReceiveAsync<ReportRatingDetails>(ForwardMessageAsync);
         ReceiveAsync<AddOfficialToSeason>(ForwardMessageAsync);
         ReceiveAsync<ReportSeasonOfficials>(ForwardMessageAsync);
         ReceiveAsync<AssignGame>(ForwardMessageAsync);
         ReceiveAsync<UpdateHomeTeam>(ForwardMessageAsync);
         ReceiveAsync<UpdateVisitingTeam>(ForwardMessageAsync);
      }

      private static Task ForwardMessageAsync(SeasonMessage seasonMessage)
      {
         var leagueActor = FetchActorRefFor(seasonMessage.LeagueId.Value());
         leagueActor.Forward(seasonMessage);

         return Task.FromResult(leagueActor);
      }

      private static Task ForwardMessageAsync(GameMessage gameMessage)
      {
         var leagueActor = FetchActorRefFor(gameMessage.LeagueId.Value());
         leagueActor.Forward(gameMessage);

         return Task.FromResult(leagueActor);
      }

      private static Task ForwardMessageAsync(LeagueMessage leagueMessage)
      {
         var leagueActor = FetchActorRefFor(leagueMessage.LeagueId.Value());
         leagueActor.Forward(leagueMessage);

         return Task.FromResult(leagueActor);
      }

      private static Task CreateLeagueAsync(CreateLeague createLeague)
      {
         var league = Context.ActorOf(Props.Create<LeagueActor>(), createLeague.LeagueId.ToString());
         
         league.Forward(createLeague);
         
         return Task.FromResult(league);
      }

      private static IActorRef FetchActorRefFor(LeagueId leagueId)
      {
         return Context.Child(leagueId.ToString());
      }
   }
}
