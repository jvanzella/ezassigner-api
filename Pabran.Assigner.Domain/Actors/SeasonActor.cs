﻿using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Messages;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public class SeasonActor : Entity<SeasonModel>
   {
      public SeasonActor()
      {
         ReceiveAsync<ReportGameDetails>(HandleAsync);
         ReceiveAsync<SeasonCreated>(HandleAsync);
         ReceiveAsync<ReportSeasonDetails>(HandleAsync);
         ReceiveAsync<GameCreated>(HandleAsync);
         ReceiveAsync<GameTimeUpdated>(HandleAsync);
         ReceiveAsync<GameSeasonUpdated>(HandleAsync);
         ReceiveAsync<ReportGameDetails>(HandleAsync);
         ReceiveAsync<GameAssigned>(HandleAsync);
         ReceiveAsync<HomeTeamUpdated>(HandleAsync);
         ReceiveAsync<VisitingTeamUpdated>(HandleAsync);
         ReceiveAsync<DivisionAddedToSeason>(HandleAsync);
         ReceiveAsync<ReportDivisions>(HandleAsync);
         ReceiveAsync<RatingAddedToSeason>(HandleAsync);
         ReceiveAsync<ReportRatings>(HandleAsync);
         ReceiveAsync<DivisionAddedToRating>(HandleAsync);
         ReceiveAsync<ReportRatingDetails>(HandleAsync);
         ReceiveAsync<OfficialAddedToSeason>(HandleAsync);
         ReceiveAsync<ReportSeasonOfficials>(HandleAsync);
      }
      
      private Task HandleAsync(ReportSeasonOfficials reportSeasonOfficials)
      {
         var officialsDto = new SeasonOfficialsDto
         {
            Officials = State.Officials.Select(o => new SeasonOfficialDto
            {
               OfficialId = o.UserId.Id,
               Rating = o.Rating.Name
            }).ToArray()
         };

         Sender.Tell(officialsDto, Self);

         return Task.FromResult(officialsDto);
      }

      private Task HandleAsync(OfficialAddedToSeason officialAdded)
      {
         State.AddOfficial(officialAdded.UserId, officialAdded.Rating);

         return Task.FromResult(true);
      }

      private Task HandleAsync(ReportRatingDetails reportRating)
      {
         var rating = State.Ratings.First(r => r.Name == reportRating.RatingId);

         var ratingDto = new RatingDto
         {
            Name = rating.Name,
            Divisions = rating.Divisions.Select(r => r.Name).ToArray()
         };

         Sender.Tell(ratingDto);

         return Task.FromResult(ratingDto);
      }

      private Task HandleAsync(DivisionAddedToRating addDivisionToRating)
      {
         State.AddDivisionToRating(
            addDivisionToRating.Rating.Value(),
            addDivisionToRating.DivisionId.Value());

         return Task.FromResult(true);
      }

      private Task HandleAsync(ReportRatings reportRatings)
      {
         var ratings = new RatingsDto
         {
            Ratings = State.Ratings.Select(r => r.Name).ToArray()
         };

         Sender.Tell(ratings, Self);

         return Task.FromResult(true);
      }

      private Task HandleAsync(RatingAddedToSeason ratingAdded)
      {
         State.AddRating(ratingAdded.Rating.Value());

         return Task.FromResult(true);
      }

      private Task HandleAsync(ReportDivisions reportDivisions)
      {
         var divisionsDto = new DivisionsDto
         {
            Divisions = State.Divisions.Select(d => d.Name).ToArray()
         };

         Sender.Tell(divisionsDto, Self);

         return Task.FromResult(divisionsDto);
      }

      private Task HandleAsync(DivisionAddedToSeason divisionAdded)
      {
         State.AddDivision(divisionAdded.Division.Value());

         return Task.FromResult(true);
      }

      private static Task HandleAsync(GameMessage gameMessage)
      {
         var game = Context.Child(gameMessage.GameNumber.ToString());
         game.Forward(gameMessage);

         return Task.FromResult(true);
      }

      private static Task HandleAsync(GameCreated gameCreated)
      {
         var game = Context.ActorOf(Props.Create<GameActor>(), gameCreated.GameNumber.ToString());
         game.Forward(gameCreated);

         return Task.FromResult(game);
      }

      private Task HandleAsync(ReportSeasonDetails details)
      {
         var dto = new SeasonDto
         {
            LeagueId = State.LeagueId.Id,
            SeasonId = State.SeasonId.Id,
            Divisions = State.Divisions.Select(d => d.ToString()).ToArray()
         };

         Sender.Tell(dto, Self);

         return Task.FromResult(dto);
      }

      private Task HandleAsync(SeasonCreated seasonCreated)
      {
         if (State != null)
         {
            return Task.FromResult(false);
         }

         State = new SeasonModel();
         State.UpdateId(seasonCreated.LeagueId, seasonCreated.SeasonId);

         return Task.FromResult(State);
      }
   }
}
