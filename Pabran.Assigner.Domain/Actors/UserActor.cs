﻿using System.Linq;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public class UserActor : AggregateRoot<UserModel>
   {
      public UserActor()
      {
         Command<CreateUser>(createOfficial => Emit(new UserCreated(createOfficial.UserId)));
         Command<UpdateFirstName>(updateFirstName => Emit(new FirstNameUpdated(updateFirstName.UserId.Value(), updateFirstName.FirstName.Value())));
         Command<UpdateLastName>(updateFirstName => Emit(new LastNameUpdated(updateFirstName.UserId.Value(), updateFirstName.LastName.Value())));
         Command<UpdateEmailAddress>(updateEmailAddress => Emit(new EmailAddressUpdated(updateEmailAddress.UserId.Value(), updateEmailAddress.EmailAddress.Value())));
         Command<AddPhoneNumber>(addPhoneNumber => Emit(new PhoneNumberAdded(addPhoneNumber.UserId.Value(), addPhoneNumber.PhoneNumber.Value())));

         Command<ReportUserDetails>(Handle);
      }

      private bool Handle(ReportUserDetails report)
      {
         var userDto = new UserDto
         {
            UserId = State.Id.Id,
            FirstName = State.FirstName,
            LastName = State.LastName,
            EmailAddress = State.EmailAddress?.ToString(),
            PhoneNumbers = State.PhoneNumbers.Select(p => p.ToString()).ToArray()
         };

         Sender.Tell(userDto);

         return true;
      }

      private bool Handle(UserCreated createUser)
      {
         State = new UserModel();
         State.Update(createUser);

         return true;
      }

      private bool Handle(IDomainEvent @event)
      {
         State.Update((dynamic)@event);

         return true;
      }

      protected override bool UpdateState(IDomainEvent domainEvent)
      {
         return Handle((dynamic)domainEvent);
      }
   }
}
