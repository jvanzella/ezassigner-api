﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Infrastructure.Extensions;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public sealed class GameActor : Entity<GameModel>
   {
      public GameActor()
      {
         ReceiveAsync<GameCreated>(HandleAsync);
         ReceiveAsync<GameTimeUpdated>(HandleAsync);
         ReceiveAsync<GameSeasonUpdated>(HandleAsync);
         ReceiveAsync<ReportGameDetails>(HandleAsync);
         ReceiveAsync<GameAssigned>(HandleAsync);
         ReceiveAsync<ReportAssignments>(HandleAsync);
         ReceiveAsync<HomeTeamUpdated>(HandleAsync);
         ReceiveAsync<VisitingTeamUpdated>(HandleAsync);
      }

      private Task HandleAsync(ReportAssignments reportAssignments)
      {
         var assignmentsDto = new AssignmentsDto
         {
            Assignments =
               State.Assignments.Select(a => new AssignmentDto {OfficialId = a.UserId.Id, Position = a.Position})
                  .ToArray()
         };

         Sender.Tell(assignmentsDto);

         return Task.FromResult(true);
      }

      private Task HandleAsync(GameAssigned gameAssigned)
      {
         var assignment = new Assignment(gameAssigned.UserId.Value(), gameAssigned.Position);

         State.Assign(assignment);

         return Task.FromResult(true);
      }

      private Task HandleAsync(GameTimeUpdated gameTimeUpdated)
      {
         State.GameTime = gameTimeUpdated.GameTime.Value();

         return Task.FromResult(true);
      }

      private Task HandleAsync(GameSeasonUpdated gameSeasonUpdated)
      {
         State.SeasonId = gameSeasonUpdated.TargetSeasonId.Value();

         return Task.FromResult(true);
      }

      private Task HandleAsync(GameCreated createGame)
      {
         if (State != null)
         {
            return Task.FromResult(false);
         }

         State = new GameModel();

         State.UpdateId(createGame.LeagueId.Value(), createGame.GameNumber.Value());

         return Task.FromResult(true);
      }

      private Task HandleAsync(HomeTeamUpdated homeTeamUpdated)
      {
         State.HomeTeam = homeTeamUpdated.Team.Value();

         return Task.FromResult(true);
      }

      private Task HandleAsync(VisitingTeamUpdated visitingTeamUpdated)
      {
         State.VisitingTeam = visitingTeamUpdated.Team.Value();

         return Task.FromResult(true);
      }

      private Task HandleAsync(ReportGameDetails reportGameDetails)
      {
         var dto = new GameDto
         {
            LeagueId = State.LeagueId.Id,
            GameNumber = State.GameNumber.ToString(),
            Time = State.GameTime?.Value().ToGameTimeDto(),
            SeasonId = State.SeasonId?.Value().Id,
            HomeTeam = State.HomeTeam?.ToString(),
            VisitingTeam = State.VisitingTeam?.ToString(),
            Assignments = State.Assignments.Select(a => new AssignmentDto { OfficialId = a.UserId.Id, Position = a.Position }).ToArray()
         };

         Sender.Tell(dto);

         return Task.FromResult(dto);
      }
   }
}