﻿using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Domain;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public class FacilityActor : AggregateRoot<FacilityModel>
   {
      public FacilityActor()
      {
         Command<CreateFacility>(createFacility => Emit(new FacilityCreated(createFacility.Id.Value())));
         Command<UpdateFacilityAddress>(
            updateFacilityAddress =>
               Emit(new FacilityAddressUpdated(updateFacilityAddress.Id.Value(),
                  updateFacilityAddress.Address.Value())));
         Command<UpdateFacilityName>(
            updateFacilityName =>
                  Emit(new FacilityNameUpdated(updateFacilityName.Id.Value(), updateFacilityName.Name.Value())));
         Command<ReportFacilityDetails>(Handle);
      }

      private bool Handle(ReportFacilityDetails createFacility)
      {
         Sender.Tell(State, Self);

         return true;
      }


      private bool Handle(FacilityCreated createFacility)
      {
         if (State != null)
            return false;

         State = new FacilityModel();
         State.UpdateId(createFacility.Id.Value());

         return true;
      }

      private bool Handle(FacilityAddressUpdated facilityAddressUpdated)
      {
         State.Address = facilityAddressUpdated.Address.Value();

         return true;
      }

      private bool Handle(FacilityNameUpdated facilityNameUpdated)
      {
         State.Name = facilityNameUpdated.Name.Value();

         return true;
      }

      protected override bool UpdateState(IDomainEvent domainEvent)
      {
         return Handle((dynamic) domainEvent);
      }
   }
}