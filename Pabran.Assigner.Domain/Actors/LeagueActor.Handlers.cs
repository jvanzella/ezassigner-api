﻿using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Messages;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Actors
{
   public partial class LeagueActor
   {
      private bool Handle(GameMessage gameMessage)
      {
         var season = Context.Child(CreateActorKey(gameMessage.SeasonId.ToString()));
         season.Forward(gameMessage);

         return true;
      }

      private bool Handle(SeasonMessage seasonMessage)
      {
         var season = Context.Child(CreateActorKey(seasonMessage.SeasonId.ToString()));
         season.Forward(seasonMessage);

         return true;
      }

      private bool Handle(LeagueNameUpdated leagueNameUpdated)
      {
         State.Name = leagueNameUpdated.Name.Value();

         return true;
      }

      private bool Handle(SeasonCreated seasonCreated)
      {
         var season = Context.ActorOf(Props.Create<SeasonActor>(), CreateActorKey(seasonCreated.SeasonId.ToString()));
         season.Forward(seasonCreated);

         State.AddSeason(seasonCreated);

         return true;
      }

      private bool Handle(LeagueCreated createLeague)
      {
         if (State != null)
         {
            return false;
         }

         State = new LeagueModel();
         State.UpdateId(createLeague.LeagueId.Value());

         return true;
      }

      private string CreateActorKey(string rawKey)
      {
         return rawKey.Replace(" ", string.Empty);
      }
   }
}