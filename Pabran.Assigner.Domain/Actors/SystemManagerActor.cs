﻿// /////////////////////////////////////////////////////////
//  SystemManagerActor.cs
// 
//  Created on:      11/12/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Domain.Commands;

namespace Pabran.Assigner.Domain.Actors
{
   public sealed class SystemManagerActor : ReceiveActor
   {
      public SystemManagerActor()
      {
         ReceiveAsync<Subscribe>(subscribe =>
         {
            Context.System.EventStream.Subscribe(subscribe.Actor, subscribe.Channel);

            return Task.FromResult(true);
         });

         ReceiveAsync<Unsubscribe>(subscribe =>
         {
            Context.System.EventStream.Unsubscribe(subscribe.Actor, subscribe.Channel);

            return Task.FromResult(true);
         });
      }
   }
}