﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.Models
{
   [DataContract]
   public class SeasonDto
   {
      [DataMember(Name = "leagueId")]
      public Guid LeagueId { get; set; }
      [DataMember(Name = "seasonId")]
      public string SeasonId { get; set; }
      [DataMember(Name = "divisions", EmitDefaultValue = false)]
      public string[] Divisions { get; set; }
      [DataMember(Name = "ratings", EmitDefaultValue = false)]
      public string[] Ratings { get; set; }
   }
}