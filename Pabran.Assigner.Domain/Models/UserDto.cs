﻿// /////////////////////////////////////////////////////////
//  UserDto.cs
// 
//  Created on:      11/20/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.Models
{
   [DataContract]
   public class UserDto
   {
      [DataMember(Name = "userId")]
      public Guid UserId { get; set; }
      [DataMember(Name = "firstName")]
      public string FirstName { get; set; }
      [DataMember(Name = "lastName")]
      public string LastName { get; set; }
      [DataMember(Name = "emailAddress")]
      public string EmailAddress { get; set; }
      [DataMember(Name = "phoneNumbers")]
      public string[] PhoneNumbers { get; set; }
   }
}