﻿using System.Collections.Generic;
using System.Linq;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Events;

namespace Pabran.Assigner.Domain.Models
{
   public sealed class LeagueModel
   {
      private readonly IList<SeasonId> _seasons;
      
      public LeagueId Id { get; private set; }
      public string Name { get; set; }
      public IReadOnlyCollection<SeasonId> Seasons => _seasons.ToReadOnlyCollection();

      public LeagueModel()
      {
         _seasons = new List<SeasonId>();
      }

      public bool UpdateId(LeagueId id)
      {
         Id = id.Value();

         return true;
      }

      public bool AddSeason(SeasonCreated season)
      {
         if (_seasons.Contains(season.SeasonId))
         {
            return false;
         }

         _seasons.Add(season.SeasonId.Value());

         return true;
      }
   }
}