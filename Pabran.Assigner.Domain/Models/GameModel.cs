﻿using System.Collections.Generic;
using System.Linq;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Models
{
   public sealed class GameModel
   {
      private readonly IList<Assignment> _assignments;

      public LeagueId LeagueId { get; private set; }
      public GameNumber GameNumber { get; private set; }
      public GameTime GameTime { get; set; }
      public SeasonId SeasonId { get; set; }
      public Team HomeTeam { get; set; }
      public Team VisitingTeam { get; set; }
      public IReadOnlyCollection<Assignment> Assignments => _assignments.ToReadOnlyCollection();

      public GameModel()
      {
         _assignments = new List<Assignment>();
      }

      public bool UpdateId(LeagueId leagueId, GameNumber gameNumber)
      {
         LeagueId = leagueId.Value();
         GameNumber = gameNumber.Value();

         return true;
      }

      public void Assign(Assignment assignment)
      {
         if (_assignments.Contains(assignment))
         {
            return;
         }

         if (_assignments.Any(a => a.UserId == assignment.UserId))
         {
            return;
         }

         _assignments.Add(assignment);
      }
   }
}