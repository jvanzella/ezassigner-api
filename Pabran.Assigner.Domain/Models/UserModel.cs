﻿using System.Collections.Generic;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Events;

namespace Pabran.Assigner.Domain.Models
{
   public sealed class UserModel
   {
      private readonly IList<PhoneNumber> _phoneNumbers;

      public UserId Id { get; private set; }
      public string FirstName { get; private set; }
      public string LastName { get; private set; }
      public EmailAddress EmailAddress { get; private set; }
      public IReadOnlyCollection<PhoneNumber> PhoneNumbers => _phoneNumbers.ToReadOnlyCollection();

      public UserModel()
      {
         _phoneNumbers = new List<PhoneNumber>();
      }

      public bool Update(UserCreated userCreated)
      {
         if (Id != null)
         {
            return false;
         }

         Id = userCreated.UserId.Value();

         return true;
      }

      public void Update(FirstNameUpdated @event)
      {
         FirstName = @event.FirstName;
      }

      public void Update(LastNameUpdated @event)
      {
         LastName = @event.LastName;
      }

      public void Update(EmailAddressUpdated @event)
      {
         EmailAddress = @event.EmailAddress;
      }

      public void Update(PhoneNumberAdded @event)
      {
         if (_phoneNumbers.Contains(@event.PhoneNumber))
         {
            return;
         }

         _phoneNumbers.Add(@event.PhoneNumber);
      }
   }
}