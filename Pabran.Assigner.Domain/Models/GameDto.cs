﻿using System;
using System.Runtime.Serialization;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Models
{
   [DataContract]
   public class GameDto
   {
      [DataMember(Name = "leagueId")]
      public Guid LeagueId { get; set; }
      [DataMember(Name = "gameNumber")]
      public string GameNumber { get; set; }
      [DataMember(Name = "time")]
      public GameTimeDto Time { get; set; }
      [DataMember(Name = "season")]
      public string SeasonId { get; set; }
      [DataMember(Name = "homeTeam")]
      public string HomeTeam { get; set; }
      [DataMember(Name = "visitingTeam")]
      public string VisitingTeam { get; set; }
      [DataMember(Name = "assignments")]
      public AssignmentDto[] Assignments { get; set; }
   }
}