﻿using System.Collections.Generic;
using System.Linq;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Models
{
   public class SeasonModel
   {
      private readonly IList<Division> _divisions;
      private readonly IList<Rating> _ratings;
      private readonly IList<SeasonOfficial> _officials;

      public LeagueId LeagueId { get; private set; }
      public SeasonId SeasonId { get; private set; }

      public IReadOnlyCollection<Division> Divisions => _divisions.ToReadOnlyCollection();
      public IReadOnlyCollection<Rating> Ratings => _ratings.ToReadOnlyCollection();
      public IReadOnlyCollection<SeasonOfficial> Officials => _officials.ToReadOnlyCollection();

      public SeasonModel()
      {
         _divisions = new List<Division>();
         _ratings = new List<Rating>();
         _officials = new List<SeasonOfficial>();
      }

      public bool AddOfficial(UserId userId, string ratingId)
      {
         if (_officials.Any(f => f.UserId == userId))
         {
            return false;
         }

         var rating = _ratings.First(r => r.Name == ratingId);

         _officials.Add(new SeasonOfficial(userId, rating));

         return true;
      }

      public bool UpdateId(LeagueId leagueId, SeasonId seasonId)
      {
         LeagueId = leagueId.Value();
         SeasonId = seasonId.Value();

         return true;
      }

      public bool AddDivision(Division division)
      {
         if (_divisions.Contains(division.Value()))
         {
            return false;
         }

         _divisions.Add(division.Value());

         return true;
      }

      public bool AddRating(Rating rating)
      {
         if (_ratings.Contains(rating.Value()))
         {
            return false;
         }

         _ratings.Add(rating.Value());

         return true;
      }

      public bool AddDivisionToRating(string rating, string divisionId)
      {
         var division = _divisions.First(d => d.Name == divisionId);

         var exitingRating = _ratings.FirstOrDefault(r => r.Name.Equals(rating));

         if (exitingRating == null)
         {
            return false;
         }

         exitingRating.AddDivision(division.Value());

         return true;
      }
   }
}