﻿using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Models
{
   public sealed class FacilityModel
   {
      public FacilityId Id { get; private set; }
      public StreetAddress Address { get; set; }
      public string Name { get; set; }

      public bool UpdateId(FacilityId id)
      {
         Id = id.Value();

         return true;
      }
   }
}