﻿using Topshelf;

namespace Pabran.Assigner.AssigningService
{
   class Program
   {
      static void Main(string[] args)
      {
         HostFactory.Run(x =>
         {
            x.Service<AssignerActorService>(s =>
            {
               s.ConstructUsing(n => new AssignerActorService());
               s.WhenStarted(service => service.Start());
               s.WhenStopped(service => service.Stop());
            });

            x.RunAsLocalSystem();

            x.SetDisplayName("AssignerActorService");
            x.SetServiceName("Pabran.Assigner.AssigningService");
            x.SetDescription("Actor System for Assigner.");
            x.UseLog4Net("log4net.config");
         });
      }
   }
}
