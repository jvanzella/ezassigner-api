﻿using Pabran.Assigner.AssigningService.Messages;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.AssigningService.Responses
{
   public class SeasonDto : SeasonMessage
   {
      public SeasonDto(SeasonId id) : base(id)
      {
      }
   }
}