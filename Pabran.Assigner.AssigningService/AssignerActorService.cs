﻿using Akka.Actor;
using Pabran.Assigner.Domain.Actors;

namespace Pabran.Assigner.AssigningService
{
   public class AssignerActorService
   {
      private ActorSystem _assignerSystem;

      public void Start()
      {
         _assignerSystem = ActorSystem.Create("AssignerDomainSystem");
         _assignerSystem.ActorOf(Props.Create<LeagueManagerActor>(), "LeagueManager");
      }

      public async void Stop()
      {
         await _assignerSystem.Terminate();
      }
   }
}