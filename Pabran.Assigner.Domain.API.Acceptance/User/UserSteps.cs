﻿// /////////////////////////////////////////////////////////
//  UserSteps.cs
// 
//  Created on:      11 15, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////

using Newtonsoft.Json.Linq;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.User
{
   [Binding]
   public class UserSteps : StepBase
   {

      [Given(@"I am logged in as an assigner")]
      public void GivenIAmLoggedInAsAnAssigner()
      {
         Login("Assigner", "Password");
      }

      [Given(@"I am logged in as a system administrator")]
      public void GivenIAmLoggedInAsASystemAdministrator()
      {
         Login("SystemAdmin", "Password");
      }

      [Given(@"I am logged in as a league administrator")]
      public void GivenIAmLoggedInAsALeagueAdministrator()
      {
         Login("LeagueAdmin", "Password");
      }

      private static void Login(string username, string password)
      {
         var browser = BrowserForTest();

         var response = browser.Login(username, password);

         var token = response["access_token"].Value<string>();

         ScenarioContext.Current["bearerToken"] = token;

         var home = browser.Get("/api/v1", token);

         var homeDocument = JObject.Parse(home.Content.ReadAsStringAsync().Result);

         var personalChannel = homeDocument["personalChannel"].Value<string>();

         ScenarioContext.Current["personalChannel"] = personalChannel;
      }
   }
}