﻿Feature: AddASeasonToLeague
	In order to assign games
	As a league administrator
	I want to add a season to a league

@Acceptance
Scenario: Add a season to a League
   Given I am logged in as a league administrator
   And I have a league
   When I add a season named "Test Season" to the league
   Then the Http Status Code should be Accepted
   And the season should exist in the league
