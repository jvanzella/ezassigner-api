﻿Feature: Season maintenance
	In order to maintain the season
	As a league administrator
	I want to be able to modify league settings and children

@Acceptance
Scenario: Add a division to a season
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   When I add a division named "Test Division"
   Then the Http Status Code should be Accepted
   And the division should exist

@Acceptance
Scenario: Add a rating to a season
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   When I add a rating named "R1"
   Then the Http Status Code should be Accepted
   And the rating should exist

@Acceptance
Scenario: Add a division to a rating
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   And I have a division named "D1"
   And I have a rating named "All"
   When I add the division D1 to rating All
   Then the Http Status Code should be Accepted
   And the division should exist in the rating

@Acceptance
Scenario: Add an official to a season
   Given I am logged in as a league administrator
   And I have a league
   And I have a season   
   And I have a rating named "All"
   When I add an official with rating All to the season
   Then the Http Status Code should be Accepted
   And the official should exist in the season