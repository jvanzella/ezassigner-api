﻿using FluentAssertions;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Season
{
   public class AddSeasonToLeagueSteps : StepBase
   {
      [Then(@"the season should exist in the league")]
      public void ThenTheSeasonShouldExistInTheLeague()
      {
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var leagueId = WaitForLeagueId();

         var result = signalRService.GetObjectDetails<SeasonsDto>($"/api/v1/leagues/{leagueId}/seasons");

         signalRService.Stop();

         result.Seasons.Should().Contain(seasonId);
      }
   }
}