﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Season
{
   [Binding]
   public sealed class SeasonMaintenanceStepsSteps : StepBase
   {
      [Given(@"I have an official")]
      public void GivenIHaveAnOfficial()
      {
         WhenIAddAnOfficialWithRatingAllToTheSeason("All");
      }

      [When(@"I add a division named ""(.*)""")]
      public void WhenIAddADivisionNamed(string p0)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var model = new
         {
            DivisionId = p0
         };

         var httpContent = new StringContent(JsonConvert.SerializeObject(model));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Post($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/divisions", httpContent, token);
         
         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
         ScenarioContext.Current["divisionId"] = p0;
      }

      [Then(@"the division should exist")]
      public void ThenTheDivisionShouldExist()
      {
         var leagueId = WaitForLeagueId();
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var divisionId = (string) ScenarioContext.Current["divisionId"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();
         
         var result = signalRService.GetObjectDetails<DivisionsDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/divisions");

         signalRService.Stop();

         result.Divisions.Should().Contain(divisionId);
      }

      [Given(@"I have a division")]
      public void GivenIHaveADivision()
      {
         WhenIAddADivisionNamed("Test Division");

         ScenarioContext.Current["divisionId"] = "Test+Division";
      }

      [When(@"I add a rating named ""(.*)""")]
      public void WhenIAddARatingNamed(string p0)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var model = new
         {
            RatingId = p0
         };

         var httpContent = new StringContent(JsonConvert.SerializeObject(model));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Post($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/ratings", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
         ScenarioContext.Current["ratingId"] = p0;
      }

      [Then(@"the rating should exist")]
      public void ThenTheRatingShouldExist()
      {
         var leagueId = WaitForLeagueId();
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var ratingId = (string)ScenarioContext.Current["ratingId"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<RatingsDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/ratings/");

         signalRService.Stop();
         
         result.Ratings.Should().Contain(ratingId);
      }

      [Given(@"I have a division named ""(.*)""")]
      public void GivenIHaveADivisionNamed(string p0)
      {
         WhenIAddADivisionNamed(p0);
      }

      [Given(@"I have a rating named ""(.*)""")]
      public void GivenIHaveARatingNamed(string p0)
      {
         WhenIAddARatingNamed(p0);
      }

      [When(@"I add the division (.*) to rating (.*)")]
      public void WhenIAddTheDivisionDToRatingAll(string p0, string p1)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var model = new
         {
            DivisionId = p0
         };

         var httpContent = new StringContent(JsonConvert.SerializeObject(model));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Post($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/ratings/{p1}/divisions", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }

      [Then(@"the division should exist in the rating")]
      public void ThenTheDivisionShouldExistInTheRating()
      {
         var leagueId = WaitForLeagueId();
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var ratingId = (string)ScenarioContext.Current["ratingId"];
         var divisionId = (string) ScenarioContext.Current["divisionId"];
         
         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<RatingDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/ratings/{ratingId}");

         signalRService.Stop();

         result.Divisions.Should().Contain(divisionId);
      }

      [When(@"I add an official with rating (.*) to the season")]
      public void WhenIAddAnOfficialWithRatingAllToTheSeason(string p0)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var officialId = Guid.NewGuid();

         var model = new
         {
            OfficialId = officialId,
            Rating = p0
         };

         var httpContent = new StringContent(JsonConvert.SerializeObject(model));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Post($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/officials", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
         ScenarioContext.Current["officialId"] = officialId;
      }

      [Then(@"the official should exist in the season")]
      public void ThenTheOfficialShouldExistInTheSeason()
      {
         var leagueId = WaitForLeagueId();
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var ratingId = (string)ScenarioContext.Current["ratingId"];
         var officialId = (Guid) ScenarioContext.Current["officialId"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<SeasonOfficialsDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/officials");

         signalRService.Stop();

         var official = result.Officials.FirstOrDefault(o => o.OfficialId == officialId);

         official.Should().NotBeNull();
         official.Rating.Should().Be(ratingId);
      }
   }
}