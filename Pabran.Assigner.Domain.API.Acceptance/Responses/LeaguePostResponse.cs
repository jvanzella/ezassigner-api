﻿using System;

namespace Pabran.Assigner.Domain.API.Acceptance.Responses
{
   public class LeaguePostResponse
   {
      public Guid LeagueId { set; get; } 
   }
}