﻿using System;

namespace Pabran.Assigner.Domain.API.Acceptance.Responses
{
   public class GamePostResponse
   {
      public Guid gameNumber { set; get; } 
   }
}