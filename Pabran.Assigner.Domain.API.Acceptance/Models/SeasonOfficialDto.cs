﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class SeasonOfficialDto
   {
      [DataMember(Name = "officialId")]
      public Guid OfficialId { get; set; }
      [DataMember(Name = "rating")]
      public string Rating { get; set; }
   }
}