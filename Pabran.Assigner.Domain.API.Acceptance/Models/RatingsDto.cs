﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public sealed class RatingsDto
   {
      [DataMember(Name = "ratings")]
      public string[] Ratings { get; set; }
   }
}