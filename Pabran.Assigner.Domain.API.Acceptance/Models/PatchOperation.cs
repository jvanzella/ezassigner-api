﻿namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   internal enum PatchOperation
   {
      Test,
      Remove,
      Add,
      Replace,
      Move,
      Copy
   }
}