﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class GameTimeDto
   {
      [DataMember(Name = "startTime")]
      public SimpleDateTimeDto StartTime { get; set; }
      [DataMember(Name = "endTime")]
      public SimpleDateTimeDto EndTime { get; set; }
   }
}