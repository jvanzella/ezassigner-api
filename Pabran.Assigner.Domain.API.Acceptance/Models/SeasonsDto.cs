﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class SeasonsDto
   {
      [DataMember(Name = "seasons")]
      public string[] Seasons { get; set; }
   }
}