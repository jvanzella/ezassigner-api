﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class DivisionsDto
   {
      [DataMember(Name = "divisions")]
      public string[] Divisions { get; set; }
   }
}