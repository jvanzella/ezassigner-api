﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class SeasonOfficialsDto
   {
      [DataMember(Name = "officials")]
      public SeasonOfficialDto[] Officials { get; set; }
   }
}