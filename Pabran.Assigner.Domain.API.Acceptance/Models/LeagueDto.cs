﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Responses
{
   [DataContract]
   public class LeagueDto
   {
      [DataMember(Name = "leagueId")]
      public Guid Id { get; set; }
      [DataMember(Name = "name")]
      public string Name { get; set; }
      [DataMember(Name = "officials", EmitDefaultValue = false)]
      public string[] Officials { get; set; }
      [DataMember(Name = "divisions", EmitDefaultValue = false)]
      public string[] Divisions { get; set; }
      [DataMember(Name = "ratings", EmitDefaultValue = false)]
      public string[] Ratings { get; set; }
      [DataMember(Name = "seasons", EmitDefaultValue = false)]
      public string[] Seasons { get; set; }
   }
}