﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   public class RatingDto
   {
      [DataMember(Name = "name")]
      public string Name { get; set; }
      [DataMember(Name = "divisions")]
      public string[] Divisions { get; set; }
   }
}