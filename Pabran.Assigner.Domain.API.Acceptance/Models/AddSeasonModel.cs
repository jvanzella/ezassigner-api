﻿namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   public class AddSeasonModel
   {
      public string SeasonId { get; set; }
   }
}