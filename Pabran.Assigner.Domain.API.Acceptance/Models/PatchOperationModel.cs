﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Models
{
   [DataContract]
   internal class PatchOperationModel
   {
      [DataMember(Name = "op")]
      public PatchOperation Operation { get; set; }
      [DataMember(Name = "path")]
      public string Path { get; set; }
      [DataMember(Name = "value")]
      public object Value { get; set; }
   }
}