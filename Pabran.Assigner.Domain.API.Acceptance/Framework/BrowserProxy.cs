﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace Pabran.Assigner.Domain.API.Acceptance.Framework
{
   public class BrowserProxy
   {
      private readonly Uri _baseUri;

      public BrowserProxy(Uri baseUri)
      {
         _baseUri = baseUri;
      }

      public JObject Login(string username, string password)
      {
         using (var client = new HttpClient())
         {
            client.BaseAddress = _baseUri;

            var content = new StringContent($"grant_type=password&username={username}&password={password}");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            var response = client.PostAsync(new Uri("/authorization/token", UriKind.Relative), content).Result;

            var model = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            return model;
         }
      }

      public HttpResponseMessage Post(string endPoint, HttpContent content, string token)
      {
         using (var client = new HttpClient())
         {
            client.BaseAddress = _baseUri;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);

            return client.PostAsync(new Uri(endPoint, UriKind.Relative), content).Result;
         }
      }

      public HttpResponseMessage Patch(string endPoint, HttpContent content, string token)
      {
         using (var client = new HttpClient())
         {
            client.BaseAddress = _baseUri;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);

            var request = new HttpRequestMessage(new HttpMethod("PATCH"), new Uri(endPoint, UriKind.Relative))
            {
               Content = content
            };

            return client.SendAsync(request).Result;
         }
      }

      public HttpResponseMessage Get(string endPoint, string token)
      {
         using (var client = new HttpClient())
         {
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            client.BaseAddress = _baseUri;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);

            return client.GetAsync(new Uri(endPoint, UriKind.Relative)).Result;
         }
      }
   }
}