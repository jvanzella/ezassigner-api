﻿// /////////////////////////////////////////////////////////
//  SignalrResponse.cs
// 
//  Created on:      11 14, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////

using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Acceptance.Framework
{
   [DataContract]
   public class SignalrResponse
   {
      [DataMember(Name = "type")]
      public string Type { get; set; }
      [DataMember(Name = "payload")]
      public object Payload { get; set; }
   }
}