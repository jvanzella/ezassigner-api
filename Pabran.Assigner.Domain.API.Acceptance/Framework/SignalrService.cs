﻿// /////////////////////////////////////////////////////////
//  SignalrService.cs
// 
//  Created on:      11 14, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Diagnostics;
using System.Threading;
using FluentAssertions;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Framework
{
   public class SignalrService
   {
      private static readonly TimeSpan MaxWait = TimeSpan.FromSeconds(10);

      private readonly HubConnection _signalRConnection;
      private readonly IDictionary<string, IHubProxy> _hubProxies;
      private string _token;

      public SignalrService()
      {
         _signalRConnection = new HubConnection("http://localhost:12116/")
         {
            CookieContainer = new CookieContainer()
         };

         _hubProxies = new ConcurrentDictionary<string, IHubProxy>
         {
            ["UserHub"] = _signalRConnection.CreateHubProxy("userHubV1"),
            ["LeagueHub"] = _signalRConnection.CreateHubProxy("leagueHubV1")
         };

      }

      public void Start()
      {
         if (_signalRConnection.State != ConnectionState.Disconnected)
         {
            return;
         }

         _signalRConnection.Start().Wait();
      }

      public void Stop()
      {
         if (_signalRConnection.State != ConnectionState.Connected)
         {
            return;
         }

         _signalRConnection.Stop();
      }

      public void AddAuthorization(string bearerToken)
      {
         _token = bearerToken;

         var cookie = new Cookie("token", $"{bearerToken}")
         {
            Domain = "localhost",
            Expires = DateTime.UtcNow.AddMinutes(10)
         };
         _signalRConnection.CookieContainer.Add(cookie);
      }

      public Guid GetLeagueId(Action postAction, string personalChannel)
      {
         var hub = _hubProxies["UserHub"];


         var leagueId = Guid.Empty;
         hub.On<SignalrResponse>("handleEvent", message =>
         {
            if (message.Type != "LeagueCreated") return;

            leagueId = Guid.Parse((string)message.Payload);
         });
         
         LoginToUser(personalChannel, hub);

         postAction();

         var sw = Stopwatch.StartNew();
         while (leagueId == Guid.Empty && sw.Elapsed < TimeSpan.FromSeconds(5))
         {
            Thread.Sleep(50);
         }

         LogoutToUser(personalChannel, hub);

         return leagueId;
      }

      public T GetObjectDetails<T>(string partialUrl) where T : class
      {
         var browser = BrowserForTest();
         var result = browser.Get(partialUrl, _token);

         result.StatusCode.Should().Be(HttpStatusCode.OK);

         var obj = JsonConvert.DeserializeObject<T>(result.Content.ReadAsStringAsync().Result);

         return obj;
      }

      protected static BrowserProxy BrowserForTest()
      {
         var browser = (BrowserProxy)FeatureContext.Current["browser"];
         return browser;
      }

      protected static void LoginToUser(string personalChannel, IHubProxy hub)
      {
         hub.Invoke("Login", personalChannel).Wait();
      }

      protected static void LogoutToUser(string personalChannel, IHubProxy hub)
      {
         hub.Invoke("Logout", personalChannel).Wait();
      }
   }
}