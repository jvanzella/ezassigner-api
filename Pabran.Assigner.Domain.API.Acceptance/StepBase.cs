﻿using System;
using FluentAssertions;
using Microsoft.AspNet.SignalR.Client;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance
{
   [Binding]
   public abstract class StepBase
   {
      private static readonly TimeSpan MaxWait = TimeSpan.FromSeconds(5);

      [BeforeFeature]
      public static void BeforeFeature()
      {
         FeatureContext.Current["browser"] = new BrowserProxy(new Uri("http://localhost:12116"));
      }

      [BeforeScenario]
      public static void BeforeScenario()
      {
         ScenarioContext.Current["leagueId"] = Guid.Empty;
      }

      protected static BrowserProxy BrowserForTest()
      {
         var browser = (BrowserProxy)FeatureContext.Current["browser"];
         return browser;
      }

      protected static void CheckDates(SimpleDateTimeDto actual, DateTimeOffset expected)
      {
         actual.Year.Should().Be(expected.Year);
         actual.Month.Should().Be(expected.Month);
         actual.Day.Should().Be(expected.Day);
         actual.Hour.Should().Be(expected.Hour);
         actual.Minute.Should().Be(expected.Minute);
         actual.OffsetHours.Should().Be(expected.Offset.Hours);
      }

      protected static void LoginToLeague(Guid leagueId, IHubProxy hub)
      {
         hub.Invoke("Login", leagueId.ToString()).Wait();
      }

      protected static void LogoutToLeague(Guid leagueId, IHubProxy hub)
      {
         hub.Invoke("Logout", leagueId.ToString()).Wait();
      }

      protected static Guid WaitForLeagueId()
      {
         return (Guid)ScenarioContext.Current["leagueId"];
      }
   }
}