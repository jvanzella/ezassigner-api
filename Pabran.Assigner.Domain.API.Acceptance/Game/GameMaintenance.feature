﻿Feature: Game Maintenance
	In order to maintain the season
	As a league administrator
	I want to be able to modify league settings

@Acceptance
Scenario: Update time for a game
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   And I have a game
   When I update the game time to start at 2016/10/31 13:00:00 -8 and end at 2016/10/31 14:30:00 -8
   Then the Http Status Code should be Accepted
   And the game should start at 2016/10/31 21:00:00 -0 and end at 2016/10/31 22:30:00 -0

@Acceptance
Scenario: Update home team
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   And I have a game
   When I update the home team to be "Home Team"
   Then the Http Status Code should be Accepted
   And the home team should be "Home Team"

@Acceptance
Scenario: Update visiting team
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   And I have a game
   When I update the visiting team to be "Visiting Team"
   Then the Http Status Code should be Accepted
   And the visiting team should be "Visiting Team"