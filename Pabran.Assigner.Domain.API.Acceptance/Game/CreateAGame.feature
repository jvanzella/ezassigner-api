﻿Feature: Create a Game
	In order for assigners to assign games
	As a league administrator
	I want to be able to add games to a season

@Acceptance
Scenario: Add a game to a Season
   Given I am logged in as a league administrator
   And I have a league
   And I have a season
   When I add a new game with id "GM169999"
   Then the Http Status Code should be Accepted
   And the game should exist in the season
