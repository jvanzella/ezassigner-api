﻿using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   public partial class GameSteps
   {
      [Given(@"I have a game")]
      public void GivenIHaveAGame()
      {
         var gameNumber = (int)ScenarioContext.Current["currentGameNumber"];
         ScenarioContext.Current["currentGameNumber"] = ++gameNumber;

         WhenIAddANewGameWithId($"Game{gameNumber:D4}");
      }
   }
}