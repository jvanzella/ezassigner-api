﻿using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   [Binding]
   public sealed partial class GameSteps : StepBase
   {
      [BeforeScenario]
      public static void BeforeScenarioSetGameNumber()
      {
         ScenarioContext.Current["currentGameNumber"] = 1;
      }
   }
}
