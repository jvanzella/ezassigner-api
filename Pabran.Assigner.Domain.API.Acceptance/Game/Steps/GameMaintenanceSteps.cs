﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using Pabran.Assigner.Domain.API.Acceptance.Responses;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   [Binding]
   public sealed class GameMaintenanceStepsSteps : StepBase
   {
      [When(@"I update the home team to be ""(.*)""")]
      public void WhenIUpdateTheHomeTeamToBe(string p0)
      {
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var patchRequest = new[]
         {
            new PatchOperationModel {
               Operation = PatchOperation.Replace,
               Path = "/homeTeam",
               Value = p0
         }};

         var browser = BrowserForTest();
         var httpContent = new StringContent(JsonConvert.SerializeObject(patchRequest));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Patch($"/api/v1/leagues/{leagueId}/Seasons/{seasonId}/games/{gameNumber}", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }

      [When(@"I update the visiting team to be ""(.*)""")]
      public void WhenIUpdateTheVisitingTeamToBe(string p0)
      {
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var patchRequest = new[]
         {
            new PatchOperationModel {
               Operation = PatchOperation.Replace,
               Path = "/visitingTeam",
               Value = p0
         }};

         var browser = BrowserForTest();
         var httpContent = new StringContent(JsonConvert.SerializeObject(patchRequest));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Patch($"/api/v1/leagues/{leagueId}/Seasons/{seasonId}/games/{gameNumber}", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }

      [Then(@"the home team should be ""(.*)""")]
      public void ThenTheHomeTeamShouldBe(string p0)
      {
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<GameDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}");

         signalRService.Stop();

         result.HomeTeam.Should().Be(p0);
      }

      [Then(@"the visiting team should be ""(.*)""")]
      public void ThenTheVisitingTeamShouldBe(string p0)
      {
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<GameDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}");

         signalRService.Stop();

         result.VisitingTeam.Should().Be(p0);
      }
   }
}