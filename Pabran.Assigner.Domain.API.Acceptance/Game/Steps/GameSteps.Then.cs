﻿using System;
using FluentAssertions;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Responses;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   public partial class GameSteps
   {
      [Then(@"the game should exist in the season")]
      public void ThenTheGameShouldExistInTheSeason()
      {
         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var url = $"/api/v1/leagues/{ScenarioContext.Current["leagueId"]}/seasons/{ScenarioContext.Current["seasonId"]}/games/{ScenarioContext.Current["gameNumber"]}";

         var game = signalRService.GetObjectDetails<GameDto>(url);

         signalRService.Stop();

         game.Should().NotBeNull();
      }

      [Then(@"the game should start at (.*) and end at (.*)")]
      public void ThenTheGameShouldStartAtAndEndAt(string p0, string p1)
      {
         var startTime = DateTimeOffset.Parse(p0);
         var endTime = DateTimeOffset.Parse(p1);
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<GameDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}");

         signalRService.Stop();

         var startsAt = result.Time.StartTime;
         var endsAt = result.Time.EndTime;

         CheckDates(startsAt, startTime);
         CheckDates(endsAt, endTime);
      }

      [Then(@"the game should have its season set to ""(.*)""")]
      public void ThenTheGameShouldHaveItsSeasonSetTo(string p0)
      {
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];

         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<GameDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}");

         signalRService.Stop();

         result.Season.Should().Be(p0);
      }
   }
}