﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   public partial class GameSteps
   {
      [When(@"I add a new game with id ""(.*)""")]
      public void WhenIAddANewGameWithId(string gameNumber)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string) ScenarioContext.Current["seasonId"];

         var httpContent = new StringContent(JsonConvert.SerializeObject(new { GameNumber = gameNumber }));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var result = browser.Post($"/api/v1/leagues/{leagueId}/Seasons/{seasonId}/Games", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = result.StatusCode;
         ScenarioContext.Current.Add("gameNumber", gameNumber);
      }

      [When(@"I update the game time to start at (.*) and end at (.*)")]
      public void WhenIUpdateTheGameTimeToStartAtAndEndAt(string p0, string p1)
      {
         var startTime = DateTimeOffset.Parse(p0);
         var endTime = DateTimeOffset.Parse(p1);
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];

         var patchRequest = new[]
         {
            new PatchOperationModel {
               Operation = PatchOperation.Replace,
               Path = "/gameTime",
               Value = new
               {
                  startTime = new SimpleDateTimeDto
                  {
                     Year = startTime.Year,
                     Month = startTime.Month,
                     Day = startTime.Day,
                     Hour = startTime.Hour,
                     Minute = startTime.Minute,
                     OffsetHours = startTime.Offset.Hours
                  },
                  endTime = new SimpleDateTimeDto
                  {
                     Year = endTime.Year,
                     Month = endTime.Month,
                     Day = endTime.Day,
                     Hour = endTime.Hour,
                     Minute = endTime.Minute,
                     OffsetHours = endTime.Offset.Hours
                  }
               }
         }};

         var browser = BrowserForTest();
         var httpContent = new StringContent(JsonConvert.SerializeObject(patchRequest));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Patch($"/api/v1/leagues/{leagueId}/Seasons/{seasonId}/games/{gameNumber}", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }

   }
}