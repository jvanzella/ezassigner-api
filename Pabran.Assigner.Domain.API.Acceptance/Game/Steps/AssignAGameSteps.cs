﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using Pabran.Assigner.Domain.API.Acceptance.Responses;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.Game
{
   [Binding]
   public sealed class AssignAGameStepsSteps : StepBase
   {
      [When(@"I assign the official to position (.*)")]
      public void WhenIAssignTheOfficialIdToPosition(string p0)
      {
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var officialId = (Guid)ScenarioContext.Current["officialId"];

         var patchRequest = new[]
         {
            new PatchOperationModel {
               Operation = PatchOperation.Add,
               Path = "/assignments",
               Value = new
               {
                  officialId,
                  position = p0
               }
         }};

         var browser = BrowserForTest();
         var httpContent = new StringContent(JsonConvert.SerializeObject(patchRequest));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Patch($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
         ScenarioContext.Current["officialsPosition"] = p0;
      }

      [Then(@"the official should be assigned to the game")]
      public void ThenTheOfficialShouldBeAssignedToTheGame()
      {
         var leagueId = WaitForLeagueId();
         var gameNumber = (string)ScenarioContext.Current["gameNumber"];
         var seasonId = (string)ScenarioContext.Current["seasonId"];
         var officialId = (Guid)ScenarioContext.Current["officialId"];
         var position = (string)ScenarioContext.Current["officialsPosition"];
         
         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var result = signalRService.GetObjectDetails<GameDto>($"/api/v1/leagues/{leagueId}/seasons/{seasonId}/games/{gameNumber}");

         signalRService.Stop();
         
         var assignment = result.Assignments.First();

         assignment.OfficialId.Should().Be(officialId);
         assignment.Position.Should().Be(position);
      }

   }
}