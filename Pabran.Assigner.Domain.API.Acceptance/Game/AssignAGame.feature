﻿Feature: Assign a Game
	In order to have officials at a game
	As an assigner
	I want to be able to modify the assignments

@Acceptance
Scenario: Assign a single official to a game
   Given I am logged in as an assigner
   And I have a league
   And I have a season
   And I have a game
   And I have a rating named "All"
   And I have an official
   When I assign the official to position Official
   Then the Http Status Code should be Accepted
   And the official should be assigned to the game