﻿Feature: Create a League
	In order to assign games
	As an account administrator
	I want to be able to create a league

@Acceptance
Scenario: Add a new League
	Given I am logged in as a system administrator
	When I request a new league
	Then the Http Status Code should be Accepted
   And the league should exist
   
@Acceptance
Scenario: Update the League Name
   Given I am logged in as a league administrator
   And I have a league
   When I update the league name to "Updated Name"
   Then the league name should be "Updated Name"
   