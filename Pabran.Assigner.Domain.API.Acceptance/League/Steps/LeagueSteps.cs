﻿using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.League
{
   [Binding]
   public partial class LeagueSteps : StepBase
   {
      [BeforeScenario]
      public static void BeforeScenarioSetGameNumber()
      {
         ScenarioContext.Current["currentGameNumber"] = 1;
      }
   }
}