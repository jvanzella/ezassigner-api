﻿using System;
using System.Net;
using FluentAssertions;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Responses;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.League
{
   public partial class LeagueSteps 
   {
      [Then(@"the league should exist")]
      public void ThenTheLeagueShouldExist()
      {
         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var leagueId = WaitForLeagueId();

         var league = signalRService.GetObjectDetails<LeagueDto>($"/api/v1/leagues/{leagueId}");

         signalRService.Stop();

         league.Should().NotBeNull();
      }

      [Then(@"the Http Status Code should be (.*)")]
      public void ThenTheHttpStatusCodeShouldBe(string p0)
      {
         var statusCode = (HttpStatusCode)ScenarioContext.Current["httpStatusCode"];
         
         statusCode.Should().Be((HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), p0));
      }

      [Then(@"the league name should be ""(.*)""")]
      public void ThenTheLeagueNameShouldBe(string p0)
      {
         var signalRService = new SignalrService();
         signalRService.AddAuthorization((string)ScenarioContext.Current["bearerToken"]);
         signalRService.Start();

         var leagueId = WaitForLeagueId();

         var league = signalRService.GetObjectDetails<LeagueDto>($"/api/v1/leagues/{leagueId}");

         signalRService.Stop();

         league.Name.Should().Be(p0);
      }
   }
}