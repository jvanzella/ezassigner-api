﻿using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.League
{
   public partial class LeagueSteps
   {
      [Given(@"I have a league named ""(.*)""")]
      public void GivenIHaveALeagueNamed(string p0)
      {
         WhenIRequestANewLeagueNamed(p0);
      }

      [Given(@"I have a league")]
      public void GivenIHaveALeague()
      {
         WhenIRequestANewLeague();
      }

      [Given(@"I have a season")]
      public void GivenIHaveASeason()
      {
         WhenIAddASeasonNamedToTheLeague("TestSeason");
      }
   }
}