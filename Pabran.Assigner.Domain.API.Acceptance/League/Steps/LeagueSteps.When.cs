﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Acceptance.Framework;
using Pabran.Assigner.Domain.API.Acceptance.Models;
using TechTalk.SpecFlow;

namespace Pabran.Assigner.Domain.API.Acceptance.League
{
   public partial class LeagueSteps
   {
      [When(@"I request a new league")]
      public void WhenIRequestANewLeague()
      {
         var token = (string)ScenarioContext.Current["bearerToken"];

         var browser = BrowserForTest();
         
         var signalRService = new SignalrService();
         signalRService.AddAuthorization(token);

         signalRService.Start();
         
         var result = new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
         var leagueId = signalRService.GetLeagueId(() => result = browser.Post("/api/v1/leagues", null, token), (string)ScenarioContext.Current["personalChannel"]);

         ScenarioContext.Current["leagueId"] = leagueId;

         signalRService.Stop();

         ScenarioContext.Current.Add("httpStatusCode", result.StatusCode);
      }

      [When(@"I request a new league named ""(.*)""")]
      public void WhenIRequestANewLeagueNamed(string p0)
      {
         WhenIRequestANewLeague();
         WhenIUpdateTheLeagueNameTo(p0);
      }

      [When(@"I update the league name to ""(.*)""")]
      public void WhenIUpdateTheLeagueNameTo(string p0)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];

         var patchOperation = new[]
         {
            new PatchOperationModel
            {
               Operation = PatchOperation.Replace,
               Path = "/name",
               Value = p0
            }
         };

         var httpContent = new StringContent(JsonConvert.SerializeObject(patchOperation));
         httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Patch($"/api/v1/leagues/{leagueId}", httpContent, token);

         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }
      
      [When(@"I add a season named ""(.*)"" to the league")]
      public void WhenIAddASeasonNamedToTheLeague(string seasonName)
      {
         var browser = BrowserForTest();
         var leagueId = (Guid)ScenarioContext.Current["leagueId"];

         var content = new StringContent(JsonConvert.SerializeObject(new AddSeasonModel {SeasonId = seasonName}));
         content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

         var token = (string)ScenarioContext.Current["bearerToken"];

         var response = browser.Post($"/api/v1/leagues/{leagueId}/seasons", content, token);

         ScenarioContext.Current["seasonId"] = seasonName;
         ScenarioContext.Current["httpStatusCode"] = response.StatusCode;
      }
   }
}