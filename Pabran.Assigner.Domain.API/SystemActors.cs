﻿using Akka.Actor;

namespace Pabran.Assigner.Domain.API
{
   public static class SystemActors
   {
      public static IActorRef LeagueProcessor = ActorRefs.Nobody;
      public static IActorRef Authenticator = ActorRefs.Nobody;
      public static IActorRef SignalRActor = ActorRefs.Nobody;
   }
}