﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Pabran.Assigner.Domain.API.Models.Version1;

namespace Pabran.Assigner.Domain.API.Hubs.Version1
{
   [HubName("userHubV1")]
   [Authorize]
   public class UserHub : Hub
   {
      public void Login(string privateChannel)
      {
         Groups.Add(Context.ConnectionId, privateChannel);
      }

      public void Logout(string privateChannel)
      {
         Groups.Remove(Context.ConnectionId, privateChannel);
      }

      [HubMethodName("broadcastMessage")]
      public void BroadcastMessage(string channel, SignalrResponse message)
      {
         Clients.OthersInGroup(channel).handleEvent(message);
      }

      [HubMethodName("handleEvent")]
      public SignalrResponse HandleEvent(SignalrResponse message)
      {
         return message;
      }
   }
}