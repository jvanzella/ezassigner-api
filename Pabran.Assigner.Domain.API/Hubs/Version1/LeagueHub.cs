﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Pabran.Assigner.Domain.API.Models.Version1;

namespace Pabran.Assigner.Domain.API.Hubs.Version1
{
   [HubName("leagueHubV1")]
   [Authorize]
   public class LeagueHub : Hub
   {
      public void JoinLeague(Guid leagueId)
      {
         Groups.Add(Context.ConnectionId, leagueId.ToString());
      }

      public void LeaveLeague(Guid leagueId)
      {
         Groups.Remove(Context.ConnectionId, leagueId.ToString());
      }

      [HubMethodName("broadcastMessage")]
      public void BroadcastMessage(string channel, SignalrResponse message)
      {
         Clients.OthersInGroup(channel).handleEvent(message);
      }

      [HubMethodName("handleEvent")]
      public SignalrResponse HandleEvent(SignalrResponse message)
      {
         return message;
      }
   }
}