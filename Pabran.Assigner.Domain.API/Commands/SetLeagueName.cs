﻿using System;

namespace Pabran.Assigner.Domain.API
{
   public class SetLeagueName
   {
      public Guid LeagueId { get; private set; }
      public string Name { get; private set; }

      public SetLeagueName(Guid leagueId, string name)
      {
         LeagueId = leagueId;
         Name = name;
      }
   }
}