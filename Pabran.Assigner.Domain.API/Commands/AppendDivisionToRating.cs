﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class AppendDivisionToRating
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string RatingId { get; }
      public string DivisionId { get; }

      public AppendDivisionToRating(Guid leagueId, string seasonId, string ratingId, string divisionId)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         RatingId = ratingId;
         DivisionId = divisionId;
      }
   }
}