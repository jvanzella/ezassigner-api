﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class AppendOfficialToLeague
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public Guid Officialid { get; }
      public string Rating { get; }

      public AppendOfficialToLeague(Guid leagueId, string seasonId, Guid officialid, string rating)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         Officialid = officialid;
         Rating = rating;
      }
   }
}