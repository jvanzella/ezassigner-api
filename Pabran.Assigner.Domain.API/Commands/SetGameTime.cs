﻿using System;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.API
{
   public class SetGameTime
   {
      public Guid LeagueId { get;  }
      public string SeasonId { get; }
      public string GameNumber { get; }
      public GameTimeDto GameTime { get; }

      public SetGameTime(Guid leagueId, string seasonId, string gameNumber, GameTimeDto gameTime)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
         GameTime = gameTime;
      }
   }
}