﻿using Pabran.Assigner.Domain.Infrastructure.Domain;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class CheckAuthentication : ICommand
   {
      public string UserName { get; set; }
      public string Password { get; set; }
   }
}