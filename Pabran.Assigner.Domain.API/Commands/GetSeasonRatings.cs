﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public sealed class GetSeasonRatings : SignalRMessage
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }

      public GetSeasonRatings(Guid leagueId, string seasonId, string responseChannel) 
         : base(responseChannel)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
      }
   }
}