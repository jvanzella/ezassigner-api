﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class NewRating
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string RatingId { get; }

      public NewRating(Guid leagueId, string seasonId, string ratingId)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         RatingId = ratingId;
      }
   }
}