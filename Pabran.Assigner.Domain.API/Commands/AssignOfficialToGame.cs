﻿using System;
using Pabran.Assigner.Domain.API.Models.Version1;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class AssignOfficialToGame
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string GameNumber { get; }
      public AddOfficialToGameDto Assignment { get; }

      public AssignOfficialToGame(Guid leagueId, string seasonId, string gameNumber, AddOfficialToGameDto assignment)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
         Assignment = assignment;
      }
   }
}