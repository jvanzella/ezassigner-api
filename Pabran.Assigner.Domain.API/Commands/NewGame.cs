﻿using System;

namespace Pabran.Assigner.Domain.API
{
   public class NewGame
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string GameNumber { get; }

      public NewGame(Guid leagueId, string seasonId, string gameNumber)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
      }
   }
}