﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class SetGameSeason
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string GameNumber { get; }
      public string TargetSeasonId { get; }

      public SetGameSeason(Guid leagueId, string seasonId, string gameNumber, string targetSeasonId)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
         TargetSeasonId = targetSeasonId;
      }
   }
}