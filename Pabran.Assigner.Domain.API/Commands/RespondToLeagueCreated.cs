﻿// /////////////////////////////////////////////////////////
//  RespondToLeagueCreated.cs
// 
//  Created on:      11/14/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class RespondToLeagueCreated : SignalRMessage
   {
      public LeagueId LeagueId { get; }

      public RespondToLeagueCreated( LeagueId leagueId) 
         : base(leagueId.ToString())
      {
         LeagueId = leagueId.Value();
      }
   }
}