﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class GetSeasonOfficials : SignalRMessage
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }

      public GetSeasonOfficials(Guid leagueId, string seasonId, string responseChannel)
         : base(responseChannel)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
      }
   }
}