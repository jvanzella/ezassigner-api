﻿// /////////////////////////////////////////////////////////
//  SetTeam.cs
// 
//  Created on:      11/18/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public abstract class SetTeam
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string GameNumber { get; }
      public string TeamName { get; }

      protected SetTeam(Guid leagueId, string seasonId, string gameNumber, string teamName)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
         TeamName = teamName;
      }
   }
}