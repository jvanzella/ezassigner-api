﻿// /////////////////////////////////////////////////////////
//  SetHomeTeam.cs
// 
//  Created on:      11/18/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class SetHomeTeam : SetTeam
   {
      public SetHomeTeam(Guid leagueId, string seasonId, string gameNumber, string teamName) 
         : base(leagueId, seasonId, gameNumber, teamName)
      {
      }
   }
}