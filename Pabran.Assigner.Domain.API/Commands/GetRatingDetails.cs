﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class GetRatingDetails : SignalRMessage
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string RatingId { get; }

      public GetRatingDetails(Guid leagueId, string seasonId, string ratingId, string responseChannel)
         : base(responseChannel)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         RatingId = ratingId;
      }
   }
}