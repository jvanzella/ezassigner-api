﻿using System;

namespace Pabran.Assigner.Domain.API
{
   public class NewSeason
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }

      public NewSeason(Guid leagueId, string seasonId)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
      }
   }
}