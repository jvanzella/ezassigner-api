﻿using System;

namespace Pabran.Assigner.Domain.API
{
   public class GetLeagueSeasons
   {
      public Guid LeagueId { get; }
      public string ResponseChannel { get; }

      public GetLeagueSeasons(Guid leagueId, string responseChannel)
      {
         LeagueId = leagueId;
         ResponseChannel = responseChannel;
      }
   }
}