﻿#region Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
//  SignalRMessage.cs
// 
//  Created on:      11 10, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
#endregion
namespace Pabran.Assigner.Domain.API.Commands
{
   public class SignalRMessage
   {
      public string Channel { get; }

      public SignalRMessage(string channel)
      {
         Channel = channel;
      }
   }
}