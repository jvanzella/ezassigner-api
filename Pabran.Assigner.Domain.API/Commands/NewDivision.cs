﻿using System;

namespace Pabran.Assigner.Domain.API.Commands
{
   public class NewDivision
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string DivisionId { get; }

      public NewDivision(Guid leagueId, string seasonid, string divisionId)
      {
         LeagueId = leagueId;
         SeasonId = seasonid;
         DivisionId = divisionId;
      }
   }
}