﻿using System;
using Pabran.Assigner.Domain.API.Commands;

namespace Pabran.Assigner.Domain.API
{
   public sealed class GetLeagueDetails : SignalRMessage
   {
      public Guid Id { get; private set; }
      
      public GetLeagueDetails(Guid id, string responseChannel)
         : base(responseChannel)
      {
         Id = id;
      }
   }
}