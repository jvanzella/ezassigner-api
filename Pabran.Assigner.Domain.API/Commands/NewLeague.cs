﻿using System;

namespace Pabran.Assigner.Domain.API
{
   public class NewLeague
   {
      public Guid Id { get; }

      public NewLeague(Guid id)
      {
         Id = id;
      }
   }
}