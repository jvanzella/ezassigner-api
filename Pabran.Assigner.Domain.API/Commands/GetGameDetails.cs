﻿using System;
using Pabran.Assigner.Domain.API.Commands;

namespace Pabran.Assigner.Domain.API
{
   public class GetGameDetails : SignalRMessage
   {
      public Guid LeagueId { get; }
      public string SeasonId { get; }
      public string GameNumber { get; }

      public GetGameDetails(Guid leagueId, string seasonId, string gameNumber, string responseChannel)
         : base(responseChannel)
      {
         LeagueId = leagueId;
         SeasonId = seasonId;
         GameNumber = gameNumber;
      }
   }
}