﻿// /////////////////////////////////////////////////////////
//  SignalRActor.cs
// 
//  Created on:      11 10, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Akka.Actor;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json.Linq;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.API.Models.Version1;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Messages;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.API.Actors
{
   public sealed class SignalRActor : ReceiveActor, IDisposable
   {
      private static readonly object Locker = new object();
      private static ActorSelection _systemManager;
      private static HubConnection _hubConnection;
      private static IDictionary<string, IHubProxy> _hubProxies;

      static SignalRActor()
      {
         CreateSystemManager();
         CreateHubConnection();
         CreateHubProxies();

         StartConnection();
      }

      private static JObject Login(string username, string password)
      {
         using (var client = new HttpClient())
         {
            client.BaseAddress = new Uri("http://localhost:12116/");

            var content = new StringContent($"grant_type=password&username={username}&password={password}");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            var response = client.PostAsync(new Uri("/authorization/token", UriKind.Relative), content).Result;

            var model = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            return model;
         }
      }

      private static void StartConnection()
      {
         if (_hubConnection.State == ConnectionState.Connected)
         {
            return;
         }

         lock (Locker)
         {
            if (_hubConnection.State != ConnectionState.Connected)
            {
               _hubConnection.Start().Wait();
            }
         }
      }

      private static void CreateHubProxies()
      {
         if (_hubProxies != null)
         {
            return;
         }

         lock (Locker)
         {
            if (_hubProxies == null)
            {
               _hubProxies = new ConcurrentDictionary<string, IHubProxy>
               {
                  ["UserHub"] = _hubConnection.CreateHubProxy("userHubV1"),
                  ["LeagueHub"] = _hubConnection.CreateHubProxy("leagueHubV1")
               };
            }
         }
      }

      private static void CreateHubConnection()
      {
         if (_hubConnection != null)
         {
            return;
         }

         lock (Locker)
         {
            if (_hubConnection != null) return;

            _hubConnection = new HubConnection("http://localhost:12116/")
            {
               CookieContainer = new CookieContainer()
            };

            var token = Login("SysAdmin", "Password");

            var cookie = new Cookie("token", $"{token["access_token"].Value<string>()}")
            {
               Domain = "localhost",
               Expires = DateTime.UtcNow.AddMinutes(10)
            };
            _hubConnection.CookieContainer.Add(cookie);
         }
     }

      private static void CreateSystemManager()
      {
         if (_systemManager != null)
         {
            return;
         }

         lock (Locker)
         {
            if (_systemManager == null)
            {
               _systemManager = 
                  Context.System.ActorSelection("akka.tcp://AssignerDomainSystem@localhost:8091/user/SystemManager");
            }
         }
      }

      public SignalRActor()
      {
         ReceiveAsync<RespondToLeagueCreated>(SendSignalRMessage);
         ReceiveAsync<LeagueNameUpdated>(SendSignalRMessage);

         _systemManager.Tell(new Subscribe(Self, typeof(LeagueNameUpdated)));
      }

      private static Task SendSignalRMessage(LeagueMessage arg)
      {
         var leagueHub = _hubProxies["LeagueHub"];
         
         var message = new SignalrResponse
         {
            Type = arg.GetType().Name,
            Payload = arg
         };

         return leagueHub.Invoke("broadcastMessage", arg.LeagueId, message);
      }

      private static Task SendSignalRMessage(RespondToLeagueCreated arg)
      {
         var userHub = _hubProxies["UserHub"];

         var message = new SignalrResponse
         {
            Type = "LeagueCreated",
            Payload = arg.LeagueId.Id
         };
         
         return userHub.Invoke("broadcastMessage", arg.Channel, message);
      }

      public void Dispose()
      {
         _hubConnection.Dispose();
      }
   }
}