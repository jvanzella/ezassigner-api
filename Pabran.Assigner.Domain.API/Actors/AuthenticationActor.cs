﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.API.Models.Version1.Authentication;

namespace Pabran.Assigner.Domain.API.Actors
{
   public sealed class AuthenticationActor : ReceiveActor
   {
      public AuthenticationActor()
      {
         ReceiveAsync<CheckAuthentication>(Handler);
      }

      private Task Handler(CheckAuthentication checkAuthentication)
      {
         Sender.Tell(
            new AuthenicationResult
            {
               Status = AuthenticationStatus.Ok,
               Identity =
                  new UserIdentity
                  {
                     Id = 1,
                     Name = checkAuthentication.UserName,
                     Claims = new []
                     {
                        new ClaimIdentity { Id = 1, Type = "CreateLeague", Value = string.Empty },
                        new ClaimIdentity { Id = 2, Type = "ViewLeague", Value = "7E56EE2D-05CD-4468-9171-384A429DA218" }}
                  }
            }, Self);

         return Task.FromResult(true);
      }
   }
}