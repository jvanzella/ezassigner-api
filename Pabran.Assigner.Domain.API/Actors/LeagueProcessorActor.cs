﻿using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Infrastructure.Extensions;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.API.Actors
{
   public class LeagueProcessorActor : ReceiveActor
   {
      private static readonly object Locker = new object();
      private static readonly ActorSelection LeagueManager;
      
      static LeagueProcessorActor()
      {
         if (LeagueManager != null)
         {
            return;
         }

         lock (Locker)
         {
            if (LeagueManager != null) return;

               LeagueManager =
                  Context.System.ActorSelection("akka.tcp://AssignerDomainSystem@localhost:8091/user/LeagueManager");
         }
      }

      public LeagueProcessorActor()
      {
         ReceiveAsync<GetLeagueDetails>(async getLeague =>
         {
            var leagueId = new LeagueId(getLeague.Id);

            var leagueDto = await AskLeagueManager<LeagueDto>(new ReportLeagueDetails(leagueId.Value()));

            Sender.Tell(leagueDto);
         });

         ReceiveAsync<NewLeague>(newLeague =>
         {
            var leagueId = new LeagueId(newLeague.Id);

            TellLeagueManager(new CreateLeague(leagueId.Value()));
            
            SystemActors.SignalRActor.Tell(new RespondToLeagueCreated(leagueId));

            return Task.FromResult(leagueId);
         });

         ReceiveAsync<NewGame>(newGame =>
         {
            var leagueId = new LeagueId(newGame.LeagueId);
            var seasonId = new SeasonId(newGame.SeasonId);
            var gameNumber = new GameNumber(newGame.GameNumber);

            TellLeagueManager(new CreateGame(leagueId.Value(), seasonId.Value(), gameNumber.Value()));

            return Task.FromResult(gameNumber);
         });

         ReceiveAsync<GetGameDetails>(async getGameDetails =>
         {
            var leagueId = new LeagueId(getGameDetails.LeagueId);
            var seasonId = new SeasonId(getGameDetails.SeasonId);
            var gameNumber = new GameNumber(getGameDetails.GameNumber);

            var gameDto = await AskLeagueManager<GameDto>(new ReportGameDetails(leagueId.Value(), seasonId.Value(), gameNumber.Value()));
            
            Sender.Tell(gameDto);
         });

         ReceiveAsync<SetLeagueName>(setLeagueName =>
         {
            TellLeagueManager(new UpdateLeagueName(new LeagueId(setLeagueName.LeagueId), setLeagueName.Name));

            return Task.FromResult(true);
         });

         ReceiveAsync<SetGameTime>(setGameTime =>
         {
            TellLeagueManager(
               new UpdateGameTime(
                  new LeagueId(setGameTime.LeagueId), 
                  new SeasonId(setGameTime.SeasonId), 
                  new GameNumber(setGameTime.GameNumber), 
                  setGameTime.GameTime.ToGameTime()
                  ));

            return Task.FromResult(true);
         });

         ReceiveAsync<NewSeason>(newSeason =>
         {
            TellLeagueManager(new CreateSeason(new LeagueId(newSeason.LeagueId), new SeasonId(newSeason.SeasonId)));

            return Task.FromResult(true);
         });

         ReceiveAsync<GetLeagueSeasons>(async getLeagueSeasons =>
         {
            var leagueId = new LeagueId(getLeagueSeasons.LeagueId);

            var seasonsDto = await AskLeagueManager<SeasonsDto>(new ReportSeasons(leagueId.Value()));

            Sender.Tell(seasonsDto);
         });

         ReceiveAsync<SetGameSeason>(setGameSeason =>
         {
            TellLeagueManager(new UpdateGameSeason(
               new LeagueId(setGameSeason.LeagueId),
               new SeasonId(setGameSeason.SeasonId),
               new GameNumber(setGameSeason.GameNumber), 
               new SeasonId(setGameSeason.TargetSeasonId)));

            return Task.FromResult(true);
         });

         ReceiveAsync<NewDivision>(newDivision =>
         {
            TellLeagueManager(new AddDivisionToSeason(
               new LeagueId(newDivision.LeagueId), 
               new SeasonId(newDivision.SeasonId), 
               new Division(newDivision.DivisionId)));

            return Task.FromResult(true);
         });

         ReceiveAsync<GetSeasonDivisions>(async getSeasonDivisions =>
         {
            var divisionsDto = await AskLeagueManager<DivisionsDto>(new ReportDivisions(
               new LeagueId(getSeasonDivisions.LeagueId),
               new SeasonId(getSeasonDivisions.SeasonId)));

            Sender.Tell(divisionsDto);
         });

         ReceiveAsync<NewRating>(newRating =>
         {
            TellLeagueManager(new AddRatingToSeason(
               new LeagueId(newRating.LeagueId),
               new SeasonId(newRating.SeasonId),
               new Rating(newRating.RatingId, Enumerable.Empty<Division>())));

            return Task.FromResult(true);
         });

         ReceiveAsync<GetSeasonRatings>(async getSeasonRatings =>
         {
            var ratingsDto = await AskLeagueManager<RatingsDto>(new ReportRatings(
               new LeagueId(getSeasonRatings.LeagueId),
               new SeasonId(getSeasonRatings.SeasonId)));

            Sender.Tell(ratingsDto);
         });

         ReceiveAsync<AppendDivisionToRating>(addDivisionToRating =>
         {
            TellLeagueManager(new AddDivisionToRating(
               new LeagueId(addDivisionToRating.LeagueId), 
               new SeasonId(addDivisionToRating.SeasonId), 
               addDivisionToRating.DivisionId,
               addDivisionToRating.RatingId));

            return Task.FromResult(true);
         });

         ReceiveAsync<GetRatingDetails>(async getRatingDetails =>
            {
               var ratingDto = await AskLeagueManager<RatingDto>(new ReportRatingDetails(
                  new LeagueId(getRatingDetails.LeagueId),
                  new SeasonId(getRatingDetails.SeasonId),
                  getRatingDetails.RatingId));

               Sender.Tell(ratingDto);
            }
         );

         ReceiveAsync<AppendOfficialToLeague>(addOfficialToLeague =>
         {
            TellLeagueManager(new AddOfficialToSeason(
               new LeagueId(addOfficialToLeague.LeagueId), 
               new SeasonId(addOfficialToLeague.SeasonId),
               new UserId(addOfficialToLeague.Officialid), 
               addOfficialToLeague.Rating));

            return Task.FromResult(true);
         });

         ReceiveAsync<GetSeasonOfficials>(async getSeasonOfficials =>
         {
            var seasonOfficialsDto = await AskLeagueManager<SeasonOfficialsDto>(new ReportSeasonOfficials(
               new LeagueId(getSeasonOfficials.LeagueId),
               new SeasonId(getSeasonOfficials.SeasonId)));

            Sender.Tell(seasonOfficialsDto);
         });

         ReceiveAsync<AssignOfficialToGame>(assignOfficial =>
         {
            TellLeagueManager(new AssignGame(
               new LeagueId(assignOfficial.LeagueId),
               new SeasonId(assignOfficial.SeasonId),
               new GameNumber(assignOfficial.GameNumber),
               new UserId(assignOfficial.Assignment.OfficialId),
               assignOfficial.Assignment.Position
            ));

            return Task.FromResult(true);
         });

         ReceiveAsync<SetHomeTeam>(setHomeTeam =>
         {
            TellLeagueManager(new UpdateHomeTeam(
               new LeagueId(setHomeTeam.LeagueId), 
               new SeasonId(setHomeTeam.SeasonId), 
               new GameNumber(setHomeTeam.GameNumber), 
               new Team(setHomeTeam.TeamName)));

            return Task.FromResult(true);
         });

         ReceiveAsync<SetVisitingTeam>(setVisitingTeam =>
         {
            TellLeagueManager(new UpdateVisitingTeam(
               new LeagueId(setVisitingTeam.LeagueId),
               new SeasonId(setVisitingTeam.SeasonId),
               new GameNumber(setVisitingTeam.GameNumber),
               new Team(setVisitingTeam.TeamName)));

            return Task.FromResult(true);
         });
      }

      private void TellLeagueManager(object messasge)
      {
         LeagueManager.Tell(messasge, Self);
      }

      private Task<T> AskLeagueManager<T>(object messasge)
      {
         return LeagueManager.Ask<T>(messasge);
      }
   }
}
