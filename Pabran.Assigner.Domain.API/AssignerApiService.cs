﻿using System;
using Microsoft.Owin.Hosting;

namespace Pabran.Assigner.Domain.API
{
   public class AssignerApiService
   {
      private IDisposable _host;
      public void Start()
      {
         var url = "http://localhost:12116";
         _host = WebApp.Start<Startup>(url);
      }

      public void Stop()
      {
         _host.Dispose();
      }
   }
}