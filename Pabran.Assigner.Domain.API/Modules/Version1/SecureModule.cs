﻿#region Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
//  SecureModule.cs
// 
//  Created on:      11 07, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
#endregion

using Nancy;
using Nancy.Security;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
   public abstract class SecureModule : NancyModule
   {
      protected SecureModule(string modulePath)
         : base(modulePath)
      {
#if !DEBUG
         this.RequiresHttps();
#endif
         this.RequiresAuthentication();
      }
   }
}