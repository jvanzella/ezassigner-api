﻿using System.Linq;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
   public class IndexModule : SecureModule
   {
      public IndexModule()
         : base("/v1")
      {
         Get("", args => new
         {
         });
      }
   }
}