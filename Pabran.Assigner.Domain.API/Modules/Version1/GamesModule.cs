﻿using System.Linq;
using Akka.Actor;
using Akka.Util.Internal;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.API.Models.Version1;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
   public sealed class GamesModule : SecureModule
   {
      public GamesModule()
         : base("/v1/leagues/{LeagueId}/seasons/{SeasonId}/games")
      {
         Post("/", _ =>
         {
            var dto = this.Bind<GamePostRequest>();

            SystemActors.LeagueProcessor.Tell(new NewGame(dto.LeagueId, dto.SeasonId, dto.GameNumber));

            return HttpStatusCode.Accepted;
         });

         Get("/{GameNumber}", async (parameters, cancellationToken) =>
         {
            var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

            var game = await SystemActors.LeagueProcessor.Ask<GameDto>(new GetGameDetails(
               parameters.LeagueId, 
               parameters.SeasonId,
               parameters.GameNumber,
               channel));

            return game;
         });

         Patch("/{GameNumber}", parameters =>
         {
            var patchModel = new GamePatchRequest
            {
               LeagueId = parameters.LeagueId,
               SeasonId = parameters.SeasonId,
               GameNumber = parameters.GameNumber,
               Operations = JsonConvert.DeserializeObject<PatchRequestOperation[]>(Request.Body.AsString())
            };

            ProcessGamePatchCommands(patchModel);

            return HttpStatusCode.Accepted;
         });
      }

      private static void ProcessGamePatchCommands(GamePatchRequest patchRequestArguments)
      {
         patchRequestArguments.Operations.ForEach(o =>
         {
            switch (o.Path)
            {
               case "/gameTime":
                  if(o.Operation == PatchOperation.Replace)
                  {
                     var valueAsJObject = (JObject)o.Value;
                     SystemActors.LeagueProcessor.Tell(
                        new SetGameTime(
                           patchRequestArguments.LeagueId,
                           patchRequestArguments.SeasonId,
                           patchRequestArguments.GameNumber,
                           valueAsJObject.ToObject<GameTimeDto>()));
                  }
                  break;
               case "/season":
                  if (o.Operation == PatchOperation.Replace)
                  {
                     SystemActors.LeagueProcessor.Tell(
                        new SetGameSeason(
                           patchRequestArguments.LeagueId,
                           patchRequestArguments.SeasonId,
                           patchRequestArguments.GameNumber,
                           (string)o.Value
                        ));
                  }
                  break;
               case "/assignments":
                  if (o.Operation == PatchOperation.Add)
                  {
                     var valueAsJObject = (JObject)o.Value;
                     SystemActors.LeagueProcessor.Tell(
                        new AssignOfficialToGame(
                           patchRequestArguments.LeagueId,
                           patchRequestArguments.SeasonId,
                           patchRequestArguments.GameNumber,
                           valueAsJObject.ToObject<AddOfficialToGameDto>()));
                  }
                  break;
               case "/homeTeam":
                  if (o.Operation == PatchOperation.Replace)
                  {
                     SystemActors.LeagueProcessor.Tell(
                        new SetHomeTeam(
                           patchRequestArguments.LeagueId,
                           patchRequestArguments.SeasonId,
                           patchRequestArguments.GameNumber,
                           (string)o.Value
                        ));
                  }
                  break;
               case "/visitingTeam":
                  if (o.Operation == PatchOperation.Replace)
                  {
                     SystemActors.LeagueProcessor.Tell(
                        new SetVisitingTeam(
                           patchRequestArguments.LeagueId,
                           patchRequestArguments.SeasonId,
                           patchRequestArguments.GameNumber,
                           (string)o.Value
                        ));
                  }
                  break;
            }
         });
      }
   }
}