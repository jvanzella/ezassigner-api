﻿using System.Linq;
using Akka.Actor;
using Nancy;
using Nancy.ModelBinding;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.API.Models.Version1;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
   public sealed class SeasonsModule : SecureModule
   {
      public SeasonsModule()
         : base("/v1/leagues/{LeagueId:guid}/seasons")
      {

         Post("/", _ =>
         {
            var seasonPostRequest = this.Bind<SeasonPostRequest>();

            SystemActors.LeagueProcessor.Tell(new NewSeason(seasonPostRequest.LeagueId, seasonPostRequest.SeasonId));

            return HttpStatusCode.Accepted;
         });

         Get("/", async (parameters, cancellationToken) =>
            {
               var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

               var seasons = await SystemActors.LeagueProcessor.Ask<SeasonsDto>(new GetLeagueSeasons(parameters.LeagueId, channel));

               return seasons;
            });

         Post("/{SeasonId}/divisions", _ =>
         {
            var postDivisionModel = this.Bind<DivisionPostRequest>();

            SystemActors.LeagueProcessor.Tell(new NewDivision(
               postDivisionModel.LeagueId,
               postDivisionModel.SeasonId,
               postDivisionModel.DivisionId
            ));

            return HttpStatusCode.Accepted;
         });

         Get("/{SeasonId}/divisions", async (parameters, cancellationToken) =>
         {
            var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

            var divisions = await SystemActors.LeagueProcessor.Ask<DivisionsDto>(new GetSeasonDivisions(
               parameters.LeagueId,
               parameters.SeasonId,
               channel));

            return divisions;
         });

         Post("/{SeasonId}/ratings", _ =>
         {
            var addRating = this.Bind<RatingPostRequest>();

            SystemActors.LeagueProcessor.Tell(new NewRating(addRating.LeagueId, addRating.SeasonId, addRating.RatingId));

            return HttpStatusCode.Accepted;
         });

         Get("/{SeasonId}/ratings", async (parameters, cancellationToken) =>
         {
            var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

            var ratings = await SystemActors.LeagueProcessor.Ask<RatingsDto>(new GetSeasonRatings(
                  parameters.LeagueId,
                  parameters.SeasonId,
                  channel));

            return ratings;
         });

         Post("/{SeasonId}/ratings/{RatingId}/divisions", _ =>
         {
            var addRating = this.Bind<RatingDivisionPostRequest>();

            SystemActors.LeagueProcessor.Tell(new AppendDivisionToRating(addRating.LeagueId, addRating.SeasonId, addRating.RatingId, addRating.DivisionId));

            return HttpStatusCode.Accepted;
         });

         Get("/{SeasonId}/ratings/{RatingId}", async (parameters, cancellationToken) =>
         {
            var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

            var rating = await SystemActors.LeagueProcessor.Ask<RatingDto>(new GetRatingDetails(
               parameters.LeagueId,
               parameters.SeasonId,
               parameters.RatingId,
               channel));

            return rating;
         });

         Post("/{SeasonId}/officials", _ =>
         {
            var addSeasonOfficial = this.Bind<AddSeasonOfficialPostRequest>();

            SystemActors.LeagueProcessor.Tell(new AppendOfficialToLeague(addSeasonOfficial.LeagueId, addSeasonOfficial.SeasonId, addSeasonOfficial.Officialid, addSeasonOfficial.Rating));

            return HttpStatusCode.Accepted;
         });

         Get("/{SeasonId}/officials", async (parameters, cancellationToken) =>
         {
            var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

            var officials = await SystemActors.LeagueProcessor.Ask<SeasonOfficialsDto>(
               new GetSeasonOfficials(
                  parameters.LeagueId,
                  parameters.SeasonId,
                  channel));

            return officials;
         });
      }
   }
}