﻿#region Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
//  UsersModule.cs
// 
//  Created on:      11 07, 2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Moneris Solutions
// /////////////////////////////////////////////////////////
#endregion

using Nancy;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
   public class UsersModule : SecureModule
   {
      public UsersModule()
         : base("/users")
      {
         
      }
   }
}