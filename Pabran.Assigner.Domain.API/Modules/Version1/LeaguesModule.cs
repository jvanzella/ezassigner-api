﻿using System;
using System.Linq;
using System.Security.Claims;
using Akka.Actor;
using Nancy;
using Nancy.Extensions;
using Nancy.Security;
using Newtonsoft.Json;
using Pabran.Assigner.Domain.API.Models.Version1;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.API.Modules.Version1
{
    public sealed class LeaguesModule : SecureModule
    {
        private bool CanViewLeague(Claim claim, Guid leagueId)
        {
            // TODO: Call to Authorization service here
            // TODO: pull this information into a local service

            return claim.Type == "ViewLeague" && Guid.Parse(claim.Value).Equals(leagueId);
        }

        public LeaguesModule()
            : base("/v1/leagues")
        {
            Get("/{LeagueId:guid}", async (parameters, cancellationToken) =>
            {
                this.RequiresClaims(claim => CanViewLeague(claim, parameters.LeagueId));

                var channel = Context.CurrentUser.Claims.First(c => c.Type == "PersonalChannel").Value;

                var league =
                    await SystemActors.LeagueProcessor.Ask<LeagueDto>(
                        new GetLeagueDetails(parameters.LeagueId, channel));

                return league;
            });

            Post("/", _ =>
            {
                this.RequiresClaims(claim => claim.Type == "CreateLeague");

                var leagueId = Guid.NewGuid();
                
                SystemActors.LeagueProcessor.Tell(new NewLeague(leagueId));

                return Response.AsJson(new LeaguePostResponse {LeagueId = leagueId}, HttpStatusCode.Accepted);
            });

            Patch("/{LeagueId:guid}", parameters =>
            {
                this.RequiresClaims(claim => claim.Subject.Name == "ChangeLeague" && Guid.Parse(claim.Value).Equals(parameters.LeagueId));

                var patchModel = new LeaguePatchRequest
                {
                    LeagueId = parameters.LeagueId,
                    Operations = JsonConvert.DeserializeObject<PatchRequestOperation[]>(Request.Body.AsString())
                };

                var firstPatchOperation = patchModel.Operations.FirstOrDefault();

                if (firstPatchOperation == null)
                    return HttpStatusCode.BadRequest;

                if (firstPatchOperation.Operation != PatchOperation.Replace && firstPatchOperation.Path != "/name")
                    return HttpStatusCode.BadRequest;

                SystemActors.LeagueProcessor.Tell(
                    new SetLeagueName(patchModel.LeagueId, (string) firstPatchOperation.Value));

                return HttpStatusCode.Accepted;
            });
        }
    }
}