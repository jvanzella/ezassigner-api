﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class AddSeasonOfficialPostRequest
   {
      public Guid LeagueId { get; set; }
      public string SeasonId { get; set; }
      public Guid Officialid { get; set; }
      public string Rating { get; set; }
   }
}