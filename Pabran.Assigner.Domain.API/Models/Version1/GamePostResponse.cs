﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   [DataContract]
   public class GamePostResponse
   {
      [DataMember(Name = "gameNumber")]
      public Guid GameNumber { set; get; } 
   }
}