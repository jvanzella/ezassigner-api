﻿// /////////////////////////////////////////////////////////
//  SignalrResponse.cs
// 
//  Created on:      11/15/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   [DataContract]
   public class SignalrResponse
   {
      [DataMember(Name = "type")]
      public string Type { get; set; }
      [DataMember(Name = "payload")]
      public object Payload { get; set; }
   }
}