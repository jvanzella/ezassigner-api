﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class AddOfficialToGameDto
   {
      public Guid OfficialId { get; set; }
      public string Position { get; set; }
   }
}