﻿namespace Pabran.Assigner.Domain.API.Models.Version1.Authentication
{
   public class Credentials
   {
      public string UserName { get; set; }

      public string Password { get; set; }
   }
}