﻿namespace Pabran.Assigner.Domain.API.Models.Version1.Authentication
{
   public class AuthenicationResult
   {
      public UserIdentity Identity { get; set; }
      public AuthenticationStatus Status { get; set; }
   }
}