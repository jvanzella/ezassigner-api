﻿namespace Pabran.Assigner.Domain.API.Models.Version1.Authentication
{
   public enum AuthenticationStatus
   {
      Ok,
      Invalid
   }
}