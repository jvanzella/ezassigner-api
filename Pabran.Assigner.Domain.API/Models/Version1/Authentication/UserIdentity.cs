﻿using System.Collections.Generic;

namespace Pabran.Assigner.Domain.API.Models.Version1.Authentication
{
   public class UserIdentity
   {
      public int Id { get; set; }

      public string Name { get; set; }

      public IReadOnlyCollection<ClaimIdentity> Claims { get; set; }
   }
}