﻿namespace Pabran.Assigner.Domain.API.Models.Version1.Authentication
{
   public class ClaimIdentity
   {
      public int Id { get; set; }

      public string Type { get; set; }

      public string Value { get; set; }
   }
}