﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class SeasonPostRequest
   {
      public Guid LeagueId { get; set; }
      public string SeasonId { get; set; }
   }
}