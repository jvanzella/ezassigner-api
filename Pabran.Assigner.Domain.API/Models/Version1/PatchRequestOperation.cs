﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   [DataContract]
   public class PatchRequestOperation
   {
      [DataMember(Name = "op")]
      public PatchOperation Operation { get; set; }
      [DataMember(Name = "path")]
      public string Path { get; set; }
      [DataMember(Name = "value")]
      public object Value { get; set; }
   }
}