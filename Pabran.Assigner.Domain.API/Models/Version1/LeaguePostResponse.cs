﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   [DataContract]
   public class LeaguePostResponse
   {
      [DataMember(Name = "leagueId")]
      public Guid LeagueId { set; get; } 
   }
}