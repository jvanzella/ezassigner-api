﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class LeaguePatchRequest
   {
      public Guid LeagueId { get; set; }
      public PatchRequestOperation[] Operations { get; set; }
   }
}