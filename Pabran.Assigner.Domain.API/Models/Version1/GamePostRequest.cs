﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class GamePostRequest
   {
      public Guid LeagueId { get; set; }
      public string SeasonId { get; set; }
      public string GameNumber { get; set; }
   }
}