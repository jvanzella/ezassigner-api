﻿using System;

namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class RatingDivisionPostRequest
   {
      public Guid LeagueId { get; set; }
      public string SeasonId { get; set; }
      public string RatingId { get; set; }
      public string DivisionId { get; set; }
   }
}