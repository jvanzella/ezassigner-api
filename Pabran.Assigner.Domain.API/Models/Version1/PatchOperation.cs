﻿namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public enum PatchOperation
   {
      Test,
      Remove,
      Add,
      Replace,
      Move,
      Copy
   }
}