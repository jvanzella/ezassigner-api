﻿namespace Pabran.Assigner.Domain.API.Models.Version1
{
   public class GamePatchRequest : LeaguePatchRequest
   {
      public string SeasonId { get; set; }
      public string GameNumber { get; set; }
   }
}