﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Pabran.Assigner.Domain.API.Security;

namespace Pabran.Assigner.Domain.API
{
   public class Startup
   {
      public void Configuration(IAppBuilder app)
      {
         SetupAuth(app);

         app
            .Map("/api", siteBuilder => siteBuilder.UseNancy())
            .MapSignalR();

         GlobalHost.HubPipeline.RequireAuthentication();
      }

      private void SetupAuth(IAppBuilder app)
      {
         // Use default options:
         app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
         {
            Provider = new OAuthTokenProvider(
                 req => req.Query.Get("bearer_token"),
                 req => req.Query.Get("access_token"),
                 req => req.Query.Get("token"),
                 req => req.Headers.Get("X-Token"),
                 req => req.Cookies["token"])
         });

         // Register a Token-based Authentication for the App:            
         app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
         {
#if DEBUG
            AllowInsecureHttp = true,
#endif
            TokenEndpointPath = new PathString("/authorization/token"),
            AccessTokenExpireTimeSpan = TimeSpan.FromHours(8),
            Provider = new SimpleAuthorizationServerProvider(),
         });
      }
   }
}