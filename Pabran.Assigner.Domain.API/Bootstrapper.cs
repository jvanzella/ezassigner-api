﻿using Akka.Actor;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Pabran.Assigner.Domain.API.Actors;

namespace Pabran.Assigner.Domain.API
{
   public class Bootstrapper : DefaultNancyBootstrapper
   {
      protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
      {
         ActorSystemRefs.ActorSystem = ActorSystem.Create("AssignerDomainAPI");

         var actorSystem = ActorSystemRefs.ActorSystem;

         SystemActors.LeagueProcessor = actorSystem.ActorOf(Props.Create<LeagueProcessorActor>(), "leagueProcessor");
         SystemActors.Authenticator = actorSystem.ActorOf(Props.Create<AuthenticationActor>(), "Authenticator");
         SystemActors.SignalRActor = actorSystem.ActorOf(Props.Create<SignalRActor>(), "SignalRActor");

         base.ApplicationStartup(container, pipelines);
      }

      protected override void Dispose(bool disposing)
      {
         ActorSystemRefs.ActorSystem.Terminate();

         base.Dispose(disposing);
      }

      protected override void ConfigureApplicationContainer(TinyIoCContainer existingContainer)
      {
         base.ConfigureApplicationContainer(existingContainer);
      }
   }
}