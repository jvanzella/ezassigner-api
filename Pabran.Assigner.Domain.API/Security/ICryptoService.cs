﻿namespace Pabran.Assigner.Domain.API.Security
{
   public interface ICryptoService
   {
      void CreateHash(byte[] data, out byte[] hash, out byte[] salt);
         
      byte[] ComputeHash(byte[] data, byte[] salt);
   }
   
}