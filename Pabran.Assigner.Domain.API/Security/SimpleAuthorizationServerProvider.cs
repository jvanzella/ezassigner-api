﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Akka.Actor;
using Microsoft.Owin.Security.OAuth;
using Pabran.Assigner.Domain.API.Commands;
using Pabran.Assigner.Domain.API.Models.Version1.Authentication;

namespace Pabran.Assigner.Domain.API.Security
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();

            return base.ValidateClientAuthentication(context);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (!CredentialsAvailable(context))
            {
                context.SetError("invalid_grant", "User or password is missing.");

                await base.GrantResourceOwnerCredentials(context);
            }

            var credentials = GetCredentials(context);

            var authenticated =
                await
                    SystemActors.Authenticator.Ask<AuthenicationResult>(new CheckAuthentication
                    {
                        UserName = credentials.UserName,
                        Password = credentials.Password
                    });

            if (authenticated.Status == AuthenticationStatus.Invalid)
            {
                context.SetError("invalid_grant", "Invalid credentials.");
                await base.GrantResourceOwnerCredentials(context);
            }

            var userIdentity = authenticated.Identity;

            var oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
            
            foreach (var claim in userIdentity.Claims)
            {
                oAuthIdentity.AddClaim(new Claim(claim.Type, claim.Value));
            }

            context.Validated(oAuthIdentity);

            await base.GrantResourceOwnerCredentials(context);
        }

        private bool CredentialsAvailable(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return
                !string.IsNullOrWhiteSpace(context.Password)
                && !string.IsNullOrWhiteSpace(context.UserName);
        }

        private Credentials GetCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return new Credentials
            {
                UserName = context.UserName,
                Password = context.Password
            };
        }
    }
}