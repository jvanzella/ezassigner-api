﻿using Topshelf;

namespace Pabran.Assigner.Domain.API
{
   class Program
   {
      static void Main(string[] args)
      {
         HostFactory.Run(x =>
         {
            x.Service<AssignerApiService>(s =>
            {
               s.ConstructUsing(n => new AssignerApiService());
               s.WhenStarted(service => service.Start());
               s.WhenStopped(service => service.Stop());
            });

            x.RunAsLocalSystem();

            x.SetDisplayName("AssignerAPIService");
            x.SetServiceName("Pabran.Assigner.Domain.API");
            x.SetDescription("API for Assigner.");
            x.UseLog4Net("log4net.config");
         });
      }
   }
}