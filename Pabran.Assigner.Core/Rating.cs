﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pabran.Assigner.Core
{
   public class Rating : ValueObject<Rating>
   {
      private readonly IList<Division> _divisions;

      public string Name { get; } 
      public IReadOnlyCollection<Division> Divisions => _divisions.ToReadOnlyCollection();

      public Rating(string name, IEnumerable<Division> divisions)
      {
         _divisions = divisions.ToList();
         Name = name;
      }

      public Task AddDivision(Division division)
      {
         if (_divisions.Contains(division.Value()))
         {
            return Task.FromResult(false);
         }

         _divisions.Add(division.Value());

         return Task.FromResult(true);
      }

      public override Rating Value()
      {
         return new Rating(Name.Value(), Divisions);
      }
   }
}