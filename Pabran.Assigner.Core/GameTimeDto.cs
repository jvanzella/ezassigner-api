﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class GameTimeDto
   {
      [DataMember(Name = "startTime")]
      public SimpleDateTimeDto StartTime { get; set; }
      [DataMember(Name = "endTime")]
      public SimpleDateTimeDto EndTime { get; set; }
   }
}