﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class DivisionsDto
   {
      [DataMember(Name = "divisions")]
      public string[] Divisions { get; set; }
   }
}