﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class SeasonOfficialsDto
   {
      [DataMember(Name = "officials")]
      public SeasonOfficialDto[] Officials { get; set; }
   }
}