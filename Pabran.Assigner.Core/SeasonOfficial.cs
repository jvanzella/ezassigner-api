﻿namespace Pabran.Assigner.Core
{
   public class SeasonOfficial : ValueObject<SeasonOfficial>
   {
      public UserId UserId { get; }
      public Rating Rating { get; }

      public SeasonOfficial(UserId userId, Rating rating)
      {
         Rating = rating;
         UserId = userId;
      }

      public override SeasonOfficial Value()
      {
         return new SeasonOfficial(UserId.Value(), Rating.Value());
      }
   }
}