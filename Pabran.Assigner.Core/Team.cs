﻿namespace Pabran.Assigner.Core
{
   public class Team : ValueObject<Team>
   {
      private readonly string _name;

      public Team(string name)
      {
         _name = name;
      }

      public override Team Value()
      {
         return new Team(_name.Value());
      }

      public override string ToString()
      {
         return _name.Value();
      }
   }
}