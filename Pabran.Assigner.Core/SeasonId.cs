﻿namespace Pabran.Assigner.Core
{
   public sealed class SeasonId : EntityId<string, SeasonId>
   {
      public SeasonId(string id) : base(id)
      {
      }

      public override SeasonId Value()
      {
         return new SeasonId(Id);
      }
   }
}