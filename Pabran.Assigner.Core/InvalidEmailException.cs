﻿// /////////////////////////////////////////////////////////
//  InvalidEmailException.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;

namespace Pabran.Assigner.Core
{
   public class InvalidEmailException  : Exception
   {
      public string EmailAddress { get; }

      public InvalidEmailException(string emailAddress)
         : base($"{emailAddress} is an invalid email address.")
      {
         EmailAddress = emailAddress;
      }
   }
}