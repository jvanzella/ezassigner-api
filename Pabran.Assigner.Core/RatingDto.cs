﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class RatingDto
   {
      [DataMember(Name = "name")]
      public string Name { get; set; }
      [DataMember(Name = "divisions")]
      public string[] Divisions { get; set; }
   }
}