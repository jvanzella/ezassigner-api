﻿namespace Pabran.Assigner.Core
{
   public sealed class Division : ValueObject<Division>
   {
      public string Name { get; }

      public Division(string name)
      {
         Name = name;
      }

      public override Division Value()
      {
         return new Division(Name.Value());
      }
      
      public override string ToString()
      {
         return $"{GetType().Name}({Name.Value()})";
      }
   }
}