﻿namespace Pabran.Assigner.Core
{
   public sealed class Season : ValueObject<Season>
   {
      public string SeasonId { get; }

      public Season(string seasonId)
      {
         SeasonId = seasonId;
      }

      public override Season Value()
      {
         return new Season(SeasonId.Value());
      }
   }
}