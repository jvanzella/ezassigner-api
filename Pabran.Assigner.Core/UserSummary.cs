﻿// /////////////////////////////////////////////////////////
//  UserSummary.cs
// 
//  Created on:      11/14/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;

namespace Pabran.Assigner.Core
{
   public class UserSummary : ValueObject<UserSummary>
   {
      public Guid UserId { get; }
      public string ResponseChannel { get; }

      public UserSummary(Guid userId, string responseChannel)
      {
         UserId = userId;
         ResponseChannel = responseChannel;
      }

      public override UserSummary Value()
      {
         return new UserSummary(new Guid(UserId.ToString()), ResponseChannel.Value());
      }
   }
}