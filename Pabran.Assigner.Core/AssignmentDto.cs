﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class AssignmentDto
   {
      [DataMember]
      public Guid OfficialId { get; set; }
      [DataMember]
      public string Position { get; set; }
   }
}