﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Pabran.Assigner.Core
{
   // Testing this is covered in EntityIdFixture
   [ExcludeFromCodeCoverage]
   public sealed class LeagueId : EntityId<Guid, LeagueId>
   {
      private LeagueId() : base(Guid.Empty)
      {
         
      }

      public LeagueId(Guid id)
         : base(id)
      {
      }

      public override LeagueId Value()
      {
         // Ensure the internal Guid is not used in the new value object
         return new LeagueId(new Guid(Id.ToString()));
      }
   }
}