﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Pabran.Assigner.Core
{
   public abstract class ValueObject<T> : IEquatable<ValueObject<T>> where T : ValueObject<T>
   {
      public virtual bool Equals(ValueObject<T> other)
      {
         if (other == null)
            return false;

         var t = GetType();
         var otherType = other.GetType();

         if (t != otherType)
            return false;

         var fields = GetFields();

         foreach (var field in fields)
         {
            var value1 = field.GetValue(other);
            var value2 = field.GetValue(this);

            if (value1 == null && value2 != null)
            {
               return false;
            }

            if (typeof(IEnumerable).IsAssignableFrom(field.FieldType) && field.FieldType != typeof(string))
            {
               var differences = new List<object>();

               var list1 = (value1 as IEnumerable<object>).ToArray();
               var list2 = (value2 as IEnumerable<object>).ToArray();

               differences.AddRange(list1.Except(list2));
               differences.AddRange(list2.Except(list1));

               return !differences.Any();
            }

            if (value1 != null && !value1.Equals(value2))
            {
               return false;
            }
         }

         return true;
      }

      public abstract T Value();

      public override bool Equals(object obj)
      {
         if (obj == null)
            return false;

         var other = obj as ValueObject<T>;

         return Equals(other);
      }

      public override int GetHashCode()
      {
         var fields = GetFields();

         const int startValue = 17;
         const int multiplier = 59;

         return fields.Select(field => field.GetValue(this))
            .Where(value => value != null)
            .Aggregate(startValue, (current, value) => current * multiplier + value.GetHashCode());
      }

      private IEnumerable<FieldInfo> GetFields()
      {
         var t = GetType();
         
         var fields = new List<FieldInfo>();

         while (t != typeof (object))
         {
            if (t == null)
            {
               break;
            }

            fields.AddRange(t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public));
            
            t = t.BaseType;
         }

         return fields;
      }

      public static bool operator ==(ValueObject<T> x, ValueObject<T> y)
      {
         return Equals(null, x) || x.Equals(y);
      }

      public static bool operator !=(ValueObject<T> x, ValueObject<T> y)
      {
         return !(x == y);
      }
   }
}