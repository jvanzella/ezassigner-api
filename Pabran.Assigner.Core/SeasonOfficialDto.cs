﻿using System;
using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class SeasonOfficialDto
   {
      [DataMember(Name = "userId")]
      public Guid OfficialId { get; set; }
      [DataMember(Name = "rating")]
      public string Rating { get; set; }
   }
}