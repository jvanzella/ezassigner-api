﻿using System;

namespace Pabran.Assigner.Core
{
   public sealed class UserId : EntityId<Guid, UserId>
   {
      public UserId(Guid id) : base(id)
      {
      }

      public override UserId Value()
      {
         // Ensure the internal Guid is not used in the new value object
         return new UserId(Guid.Parse(Id.ToString()));
      }
   }
}