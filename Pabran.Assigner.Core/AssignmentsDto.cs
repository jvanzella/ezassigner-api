﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class AssignmentsDto
   {
      [DataMember]
      public AssignmentDto[] Assignments { get; set; }
   }
}