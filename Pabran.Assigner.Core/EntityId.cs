namespace Pabran.Assigner.Core
{
   public abstract class EntityId<TId, TValueObject> : ValueObject<TValueObject> where TValueObject : ValueObject<TValueObject>
   {
      protected EntityId(TId id)
      {
         Id = id;
      }

      public TId Id { get; }

      public override string ToString()
      {
         return $"{GetType().Name}({Id})";
      }
   }
}