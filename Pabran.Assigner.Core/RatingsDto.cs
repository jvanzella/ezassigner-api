﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public sealed class RatingsDto
   {
      [DataMember(Name = "ratings")]
      public string[] Ratings { get; set; }
   }
}