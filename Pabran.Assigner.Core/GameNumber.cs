﻿using System.Diagnostics.CodeAnalysis;

namespace Pabran.Assigner.Core
{
   // Testing this is covered in EntityIdFixture
   [ExcludeFromCodeCoverage]
   public sealed class GameNumber : EntityId<string, GameNumber>
   {
      public GameNumber(string id) : base(id)
      {
      }

      public override GameNumber Value()
      {
         // Ensure the internal string is not used in the new value object
         return new GameNumber(Id.Value());
      }
   }
}