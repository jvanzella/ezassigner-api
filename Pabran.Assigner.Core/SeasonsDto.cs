﻿using System.Runtime.Serialization;

namespace Pabran.Assigner.Core
{
   [DataContract]
   public class SeasonsDto
   {
      [DataMember(Name = "seasons")]
      public string[] Seasons { get; set; }
   }
}