﻿using System;

namespace Pabran.Assigner.Core.Extensions
{
   public static class GameTimeExtensions
   {

      public static DateTimeOffset ToDateTimeOffset(this SimpleDateTimeDto date)
      {
         return new DateTimeOffset(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0, TimeSpan.FromHours(date.OffsetHours));
      }

      public static SimpleDateTimeDto ToSimpleDateTimeDto(this DateTimeOffset date)
      {
         return new SimpleDateTimeDto
         {
            Year = date.Year,
            Month = date.Month,
            Day = date.Day,
            Hour = date.Hour,
            Minute = date.Minute,
            OffsetHours = date.Offset.Hours
         };
      }
   }
}