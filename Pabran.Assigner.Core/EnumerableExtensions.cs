﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pabran.Assigner.Core
{
   public static class EnumerableExtensions
   {
      public static IReadOnlyCollection<T> ToReadOnlyCollection<T>(this IEnumerable<T> collection)
      {
         return new ReadOnlyCollection<T>(collection.ToArray());
      }
   }
}