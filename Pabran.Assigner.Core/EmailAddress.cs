﻿// /////////////////////////////////////////////////////////
//  EmailAddress.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System.Text.RegularExpressions;

namespace Pabran.Assigner.Core
{
   public sealed class EmailAddress : ValueObject<EmailAddress>
   {
      private static readonly Regex Validator = new Regex(
                                   @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                 + "@"
                                 + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$",
                                   RegexOptions.Compiled);

      private readonly string _emailAddress;

      public EmailAddress(string emailAddress)
      {
         ValidateEmailAddress(emailAddress);

         _emailAddress = emailAddress;
      }

      public override EmailAddress Value()
      {
         return new EmailAddress(_emailAddress.Value());
      }

      public override string ToString()
      {
         return _emailAddress.Value();
      }

      private static void ValidateEmailAddress(string emailAddress)
      {
         if (!Validator.IsMatch(emailAddress))
         {
            throw new InvalidEmailException(emailAddress);
         }
      }
   }
}