﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Pabran.Assigner.Core
{
   // Testing this is covered in EntityIdFixture
   [ExcludeFromCodeCoverage]
   public sealed class FacilityId : EntityId<Guid, FacilityId>
   {
      public FacilityId(Guid id) : base(id)
      {
      }

      public override FacilityId Value()
      {
         // Ensure the internal Guid is not used in the new value object
         return new FacilityId(new Guid(Id.ToString()));
      }
   }
}