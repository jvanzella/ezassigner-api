﻿// /////////////////////////////////////////////////////////
//  PhoneNumber.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;

namespace Pabran.Assigner.Core
{
   public class PhoneNumber : ValueObject<PhoneNumber>
   {
      private readonly string _phoneNumber;

      public PhoneNumber(string phoneNumber)
      {
         if (string.IsNullOrWhiteSpace(phoneNumber)) throw new ArgumentNullException(nameof(phoneNumber));

         _phoneNumber = phoneNumber;
      }

      public override PhoneNumber Value()
      {
         return new PhoneNumber(_phoneNumber);
      }

      public override string ToString()
      {
         return _phoneNumber;
      }
   }
}