﻿namespace Pabran.Assigner.Core
{
   public class Assignment : ValueObject<Assignment>
   {
      public UserId UserId { get; }
      public string Position { get; }

      public Assignment(UserId userId, string position)
      {
         Position = position;
         UserId = userId;
      }

      public override Assignment Value()
      {
         return new Assignment(UserId.Value(), Position.Value());
      }
   }
}