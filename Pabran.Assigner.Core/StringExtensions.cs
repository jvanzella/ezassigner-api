﻿namespace Pabran.Assigner.Core
{
   public static class StringExtensions
   {
      public static string Value(this string str)
      {
         return str == null ? null : string.Copy(str);
      }
   }
}