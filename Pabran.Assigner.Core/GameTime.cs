﻿using System;

namespace Pabran.Assigner.Core
{
   public class GameTime : ValueObject<GameTime>
   {
      public DateTimeOffset StartTime { get; }
      public DateTimeOffset EndTime { get; }
      public TimeSpan Duration => EndTime - StartTime;

      public GameTime(DateTimeOffset startTime, DateTimeOffset endTime)
      {
         StartTime = startTime;
         EndTime = endTime;
      }

      public override GameTime Value()
      {
         return new GameTime(StartTime.ToUniversalTime(), EndTime.ToUniversalTime());
      }
   }
}