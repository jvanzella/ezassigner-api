﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public class SeasonActorFixture : TestKit
   {
      private const string GameNumber = "SIM20161000";
      private const string DivisionName = "Gender A";
      private const string RatingName = "Beginner";
      private const string SeasonName = "TestSeason";

      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<SeasonActor>());

         sut.IsNobody().Should().BeFalse();
      }

      [Test]
      public async Task SeasonCreatedEventShouldSetIds()
      {
         var seasonId = new SeasonId(SeasonName);
         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateSeasonActor(leagueId, seasonId);

         var result = await sut.Ask<SeasonDto>(new ReportSeasonDetails(leagueId.Value(), seasonId.Value()));

         result.LeagueId.Should().Be(leagueId.Id);
         result.SeasonId.Should().Be(seasonId.Id);
      }

      [Test]
      public void AddingAGameShouldAddGameToSystem()
      {
         var seasonId = new SeasonId(SeasonName);
         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);

         var sut = CreateSeasonActor(leagueId, seasonId);

         sut.Tell(new GameCreated(leagueId.Value(), seasonId.Value(), gameNumber.Value()));

         var result = Sys.ActorSelection($"/root/{leagueId}/{seasonId}/{gameNumber}");

         result.Anchor.IsNobody().Should().BeFalse();
      }

      [Test]
      public async Task DivisionAddedToLeagueShouldBeReportedWhenDivisionsAreQueried()
      {
         var division = new Division(DivisionName);
         var seasonId = new SeasonId(SeasonName);

         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateSeasonActor(leagueId, seasonId);

         sut.Tell(new DivisionAddedToSeason(leagueId.Value(), seasonId.Value(), division.Value()));

         var answer = await sut.Ask<DivisionsDto>(new ReportDivisions(leagueId.Value(), seasonId.Value()));

         answer.Divisions.Should().Contain(DivisionName);
      }

      [Test]
      public async Task AddingARatingToLeagueShouldBeReportedWhenRatingsAreQueried()
      {
         var rating = new Rating(RatingName, Enumerable.Empty<Division>());

         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonId = new SeasonId(SeasonName);

         var sut = CreateSeasonActor(leagueId.Value(), seasonId.Value());

         sut.Tell(new RatingAddedToSeason(leagueId.Value(), seasonId.Value(), rating));

         var answer = await sut.Ask<RatingsDto>(new ReportRatings(leagueId.Value(), seasonId.Value()));

         answer.Ratings.Should().Contain(RatingName);
      }

      [Test]
      public async Task AddingADivisionToARatingShouldBeRepresentedInTheRating()
      {
         var division = new Division(DivisionName);
         var rating = new Rating(RatingName, Enumerable.Empty<Division>());

         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonId = new SeasonId(SeasonName);

         var sut = CreateSeasonActor(leagueId.Value(), seasonId.Value());

         sut.Tell(new DivisionAddedToSeason(leagueId.Value(), seasonId.Value(), division));
         sut.Tell(new RatingAddedToSeason(leagueId.Value(), seasonId.Value(), rating.Value()));
         sut.Tell(new DivisionAddedToRating(leagueId.Value(), seasonId.Value(), DivisionName, RatingName));

         var answer = await sut.Ask<RatingDto>(new ReportRatingDetails(leagueId.Value(), seasonId.Value(), RatingName));

         answer.Divisions.Should().Contain(DivisionName);
      }

      [Test]
      public async Task AnOfficialAddedShouldShowUpWhenQueried()
      {
         var division = new Division(DivisionName);
         var rating = new Rating(RatingName, Enumerable.Empty<Division>());
         var officialId = new UserId(Guid.NewGuid());
         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonId = new SeasonId(SeasonName);

         var sut = CreateSeasonActor(leagueId.Value(), seasonId.Value());

         sut.Tell(new DivisionAddedToSeason(leagueId.Value(), seasonId.Value(), division));
         sut.Tell(new RatingAddedToSeason(leagueId.Value(), seasonId.Value(), rating.Value()));
         sut.Tell(new DivisionAddedToRating(leagueId.Value(), seasonId.Value(), DivisionName, RatingName));

         var addOfficialCommand = new OfficialAddedToSeason(leagueId.Value(), seasonId.Value(), officialId.Value(), rating.Name);

         sut.Tell(addOfficialCommand);

         var answer = await sut.Ask<SeasonOfficialsDto>(new ReportSeasonOfficials(leagueId.Value(), seasonId.Value()));

         var result = answer.Officials.First();

         result.OfficialId.Should().Be(officialId.Id);
         result.Rating.Should().Be(RatingName);
      }

      private IActorRef CreateSeasonActor(LeagueId leagueId, SeasonId seasonId)
      {
         var sut = Sys.ActorOf(Props.Create<SeasonActor>());
         sut.Tell(new SeasonCreated(leagueId.Value(), seasonId.Value()));
         return sut;
      }
   }
}