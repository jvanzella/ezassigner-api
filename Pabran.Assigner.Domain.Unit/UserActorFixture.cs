﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public class UserActorFixture : TestKit
   {
      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<UserActor>());

         sut.IsNobody().Should().BeFalse();
      }

      [Test]
      public void CreatingAUserShouldEmitUserCreatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(UserCreated));

         var createOfficialCommand = new CreateUser(new UserId(Guid.NewGuid()));
         var sut = Sys.ActorOf(Props.Create<UserActor>());

         sut.Tell(createOfficialCommand);

         subscriber.ExpectMsg<UserCreated>();
      }

      [Test]
      public async Task UserCreatedShouldSetId()
      {
         var userId = new UserId(Guid.NewGuid());
         var createOfficialCommand = new CreateUser(userId.Value());
         var sut = Sys.ActorOf(Props.Create<UserActor>());

         sut.Tell(createOfficialCommand);

         var answer = await sut.Ask<UserDto>(new ReportUserDetails(userId.Value()));

         answer.UserId.Should().Be(createOfficialCommand.UserId.Id);
      }

      [Test]
      public void UpdateFirstNameCommandShouldEmitFirstNameUpdatedEvent()
      {
         var userId = new UserId(Guid.NewGuid());
         var firstName = Faker.Name.First();

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(FirstNameUpdated));

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateFirstName(userId.Value(), firstName));

         subscriber.ExpectMsg<FirstNameUpdated>();
      }

      [Test]
      public async Task FirstNameUpdatedShouldUpdateFirstName()
      {
         var userId = new UserId(Guid.NewGuid());
         var firstName = Faker.Name.First();

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateFirstName(userId.Value(), firstName));

         var answer = await sut.Ask<UserDto>(new ReportUserDetails(userId.Value()));

         answer.FirstName.Should().Be(firstName);
      }

      [Test]
      public void UpdateLastNameCommandShouldEmitLastNameUpdatedEvent()
      {
         var userId = new UserId(Guid.NewGuid());
         var lastName = Faker.Name.Last();

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(LastNameUpdated));

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateLastName(userId.Value(), lastName));

         subscriber.ExpectMsg<LastNameUpdated>();
      }

      [Test]
      public async Task LastNameUpdatedShouldUpdateLastName()
      {
         var userId = new UserId(Guid.NewGuid());
         var lastName = Faker.Name.Last();

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateLastName(userId.Value(), lastName));

         var answer = await sut.Ask<UserDto>(new ReportUserDetails(userId.Value()));

         answer.LastName.Should().Be(lastName);
      }

      [Test]
      public void UpdateEmailCommandShouldEmitEmailUpdatedEvent()
      {
         var userId = new UserId(Guid.NewGuid());
         var email = Faker.Internet.Email();

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(EmailAddressUpdated));

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateEmailAddress(userId.Value(), new EmailAddress(email)));

         subscriber.ExpectMsg<EmailAddressUpdated>();
      }

      [Test]
      public async Task UpdateEmailCommandShouldSetEmailAddress()
      {
         var userId = new UserId(Guid.NewGuid());
         var email = Faker.Internet.Email();

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new UpdateEmailAddress(userId.Value(), new EmailAddress(email)));

         var answer = await sut.Ask<UserDto>(new ReportUserDetails(userId.Value()));

         answer.EmailAddress.Should().Be(email);
      }

      [Test]
      public void AddPhoneNumberToUserShouldEmitPhoneNumberAddedEvent()
      {
         var userId = new UserId(Guid.NewGuid());
         var phoneNumber = Faker.Phone.Number();

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(PhoneNumberAdded));

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new AddPhoneNumber(userId.Value(), new PhoneNumber(phoneNumber)));

         subscriber.ExpectMsg<PhoneNumberAdded>();
      }

      [Test]
      public async Task AddPhoneNumberCommandShouldAddAPhoneNumber()
      {
         var userId = new UserId(Guid.NewGuid());
         var phoneNumber = Faker.Phone.Number();

         var sut = Sys.ActorOf(Props.Create<UserActor>());
         sut.Tell(new CreateUser(userId));
         sut.Tell(new AddPhoneNumber(userId.Value(), new PhoneNumber(phoneNumber)));

         var answer = await sut.Ask<UserDto>(new ReportUserDetails(userId.Value()));

         answer.PhoneNumbers.Should().Contain(phoneNumber);
      }
   }
}