﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public class FacilityActorFixture : TestKit
   {
      private const string FacilityName = "Facility1";

      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<FacilityActor>());

         sut.IsNobody().Should().BeFalse();
      }

      [Test]
      public async Task CreateFacilityShouldSetFields()
      {
         var facilityId = new FacilityId(Guid.NewGuid());
         var sut = CreateFacilityActor(facilityId);

         var answer = await sut.Ask<FacilityModel>(new ReportFacilityDetails(facilityId.Value()));

         answer.Should().NotBeNull();
         answer.Id.Should().Be(facilityId);
      }

      [Test]
      public void CreateFacilityShouldEmitFacilityCreatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(FacilityCreated));

         CreateFacilityActor();

         subscriber.ExpectMsg<FacilityCreated>();
      }

      [Test]
      public async Task UpdatingFacilityNameShouldSetProperty()
      {
         var facilityId = new FacilityId(Guid.NewGuid());
         var sut = CreateFacilityActor(facilityId);

         sut.Tell(new UpdateFacilityName(facilityId, FacilityName));

         var answer = await sut.Ask<FacilityModel>(new ReportFacilityDetails(facilityId.Value()));

         answer.Should().NotBeNull();
         answer.Name.Should().Be(FacilityName);
      }

      [Test]
      public void UpdateFacilityNameShouldEmitFacilityNameUpdatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(FacilityNameUpdated));

         var facilityId = new FacilityId(Guid.NewGuid());
         var sut = CreateFacilityActor(facilityId);

         sut.Tell(new UpdateFacilityName(facilityId, FacilityName));

         subscriber.ExpectMsg<FacilityNameUpdated>();
      }

      [Test]
      public async Task UpdatingFacilityAddressShouldSetProperty()
      {
         var facilityId = new FacilityId(Guid.NewGuid());
         var sut = CreateFacilityActor(facilityId);

         var address = new StreetAddress();
         sut.Tell(new UpdateFacilityAddress(facilityId, address));

         var answer = await sut.Ask<FacilityModel>(new ReportFacilityDetails(facilityId.Value()));

         answer.Should().NotBeNull();
         answer.Address.Should().Be(address);
      }

      [Test]
      public void UpdateFacilityAddressShouldEmitFacilityAddressUpdatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(FacilityAddressUpdated));

         var facilityId = new FacilityId(Guid.NewGuid());
         var sut = CreateFacilityActor(facilityId);
         var address = new StreetAddress();
         sut.Tell(new UpdateFacilityAddress(facilityId, address));

         subscriber.ExpectMsg<FacilityAddressUpdated>();
      }

      private IActorRef CreateFacilityActor()
      {
         return CreateFacilityActor(new FacilityId(Guid.NewGuid()));
      }

      private IActorRef CreateFacilityActor(FacilityId facilityId)
      {
         var createFacilityCommand = new CreateFacility(facilityId);

         var sut = Sys.ActorOf(Props.Create<FacilityActor>(), facilityId.ToString());
         sut.Tell(createFacilityCommand);

         return sut;
      }
   }
}