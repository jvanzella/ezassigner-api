﻿using System;
using Pabran.Assigner.Core;

namespace Pabran.Assigner.Domain.Unit
{
   public static class TestDataFactory
   {
      public static GameTime CreateGameTime()
      {
         var startTime = DateTime.UtcNow;
         var endTime = startTime.AddMinutes(90);
         return new GameTime(
            new DateTimeOffset(startTime.Year, startTime.Month, startTime.Day, startTime.Hour, startTime.Minute, 0, TimeSpan.Zero),
            new DateTimeOffset(endTime.Year, endTime.Month, endTime.Day, endTime.Hour, endTime.Minute, 0, TimeSpan.Zero));
      } 
   }
}