﻿using System;
using Akka.Actor;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;

namespace Pabran.Assigner.Domain.Unit
{
   public partial class LeagueActorFixture
   {
      private IActorRef CreateLeagueActor()
      {
         return CreateLeagueActor(new LeagueId(Guid.NewGuid()));
      }

      private IActorRef CreateLeagueActor(LeagueId leagueId)
      {
         var sut = Sys.ActorOf(Props.Create<LeagueActor>(), leagueId.ToString());
         sut.Tell(new CreateLeague(leagueId));
         
         return sut;
      }
   }
}