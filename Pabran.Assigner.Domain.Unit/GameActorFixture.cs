﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Infrastructure.Extensions;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public class GameActorFixture : TestKit
   {
      private const string GameNumber = "SIM20161000";
      private const string SeasonName = "Season1";
      private const string OfficialPosition = "Official";

      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<GameActor>());

         sut.IsNobody().Should().BeFalse();
      }

      [Test]
      public async Task CreatingAGameShouldSetFields()
      {
         var gameNumber = new GameNumber(GameNumber);

         var sut = CreateGameActor(gameNumber.Value());
         
         var answer = await sut.Ask<GameDto>(new ReportGameDetails(new LeagueId(Guid.Empty), new SeasonId(SeasonName), gameNumber.Value()));

         answer.Should().NotBeNull();
         answer.GameNumber.Should().Be(gameNumber.ToString());
      }

      [Test]
      public async Task UpdatingAGameTimeShouldUpdateField()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);
         var seasonId = new SeasonId(SeasonName);

         var gameTime = TestDataFactory.CreateGameTime();
         var sut = CreateGameActor(gameNumber);
         sut.Tell(new GameTimeUpdated(leagueId.Value(), seasonId.Value(), gameNumber.Value(), gameTime.Value()));

         var answer = await sut.Ask<GameDto>(new ReportGameDetails(leagueId.Value(), seasonId.Value(), gameNumber.Value()));
         
         answer.Should().NotBeNull();
         answer.Time.ToGameTime().Should().Be(gameTime);
      }

      [Test]
      public async Task UpdatingAGameSeasonShouldUpdateField()
      {
         const string newSeasonName = "Season2";

         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);
         var seasonId = new SeasonId(SeasonName);
         var newSeasonId = new SeasonId(newSeasonName);
         
         var sut = CreateGameActor(gameNumber);
         sut.Tell(new GameSeasonUpdated(leagueId.Value(), seasonId.Value(), gameNumber.Value(), newSeasonId.Value()));

         var answer = await sut.Ask<GameDto>(new ReportGameDetails(leagueId.Value(), newSeasonId.Value(), gameNumber.Value()));

         answer.Should().NotBeNull();
         answer.SeasonId.Should().Be(newSeasonId.Id);
      }

      [Test]
      public async Task UpdatingTheHomeTeamShouldChangeTheValue()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);

         var sut = CreateGameActor(gameNumber.Value());

         var team = new Team("HomeTeam");
         sut.Tell(new HomeTeamUpdated(leagueId.Value(), new SeasonId(SeasonName), new GameNumber(GameNumber), team));

         var answer = await sut.Ask<GameDto>(new ReportGameDetails(new LeagueId(Guid.Empty), new SeasonId(SeasonName), gameNumber.Value()));

         answer.HomeTeam.Should().Be(team.ToString());
      }
      
      [Test]
      public async Task UpdatingTheVistingTeamShouldChangeTheValue()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);

         var sut = CreateGameActor(gameNumber.Value());

         var team = new Team("HomeTeam");
         sut.Tell(new VisitingTeamUpdated(leagueId.Value(), new SeasonId(SeasonName), new GameNumber(GameNumber), team));

         var answer = await sut.Ask<GameDto>(new ReportGameDetails(new LeagueId(Guid.NewGuid()), new SeasonId(SeasonName), gameNumber.Value()));

         answer.VisitingTeam.Should().Be(team.ToString());
      }

      [Test]
      public async Task AssigningAGameToAnOfficialShouldAddTheAssignmentRecord()
      {
         const string position = "Official";

         var gameNumber = new GameNumber(GameNumber);
         var seasonId = new SeasonId(SeasonName);
         var officialId = new UserId(Guid.NewGuid());
         var leagueId = new LeagueId(Guid.NewGuid());

         var sut = CreateGameActor(gameNumber.Value());
         sut.Tell(new GameCreated(leagueId.Value(), seasonId.Value(), gameNumber.Value()));

         sut.Tell(new GameAssigned(leagueId.Value(), seasonId.Value(), gameNumber.Value(), officialId.Value(), position.Value()));

         var answer = await sut.Ask<AssignmentsDto>(new ReportAssignments(leagueId.Value(), seasonId.Value(), gameNumber.Value()));

         var assignment = answer.Assignments.First();

         assignment.OfficialId.Should().Be(officialId.Id);
         assignment.Position.Should().Be(position);
      }

      private IActorRef CreateGameActor()
      {
         return CreateGameActor(new GameNumber(GameNumber));
      }

      private IActorRef CreateGameActor(GameNumber gameNumber)
      {
         var leagueId = new LeagueId(Guid.NewGuid());

         var sut = Sys.ActorOf(Props.Create<GameActor>());
         sut.Tell(new GameCreated(leagueId.Value(), new SeasonId(SeasonName), gameNumber.Value()));

         return sut;
      }
   }
}