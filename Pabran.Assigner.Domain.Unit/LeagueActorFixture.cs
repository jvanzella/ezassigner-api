﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core;
using Pabran.Assigner.Domain.Actors;
using Pabran.Assigner.Domain.Commands;
using Pabran.Assigner.Domain.Events;
using Pabran.Assigner.Domain.Models;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public partial class LeagueActorFixture : TestKit
   {
      private const string LeagueName = "League1";
      private const string GameNumber = "SIM20161000";
      private const string DivisionName = "Gender A";
      private const string RatingName = "Beginner";
      private const string SeasonName = "TestSeason";

      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<LeagueActor>());

         sut.IsNobody().Should().BeFalse();
      }

      [Test]
      public async Task CreatingALeagueShouldSetId()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);

         var answer = await sut.Ask<LeagueDto>(new ReportLeagueDetails(leagueId));

         answer.Id.Should().Be(leagueId.Id);
      }

      [Test]
      public void CreatingALeagueShouldEmitLeagueCreatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(LeagueCreated));

         var sut = Sys.ActorOf(Props.Create<LeagueActor>());

         var createLeagueCommand = new CreateLeague(new LeagueId(Guid.NewGuid()));
         sut.Tell(createLeagueCommand);

         var answer = subscriber.ExpectMsg<LeagueCreated>();

         answer.LeagueId.Should().Be(createLeagueCommand.LeagueId);
      }

      [Test]
      public async Task UpdatingALeagueNameShouldSetNewName()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);
         sut.Tell(new UpdateLeagueName(leagueId, LeagueName));

         var answer = await sut.Ask<LeagueDto>(new ReportLeagueDetails(leagueId));

         answer.Name.Should().Be(LeagueName);
      }

      [Test]
      public void UpdatingALeagueNameShouldEmitLeagueNameUpdatedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(LeagueNameUpdated));

         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);
         sut.Tell(new UpdateLeagueName(leagueId, LeagueName));

         var answer = subscriber.ExpectMsg<LeagueNameUpdated>();

         answer.Name.Should().Be(LeagueName);
      }

      [Test]
      public void CreatingAGameShouldAddTheGameAndInitializeIt()
      {
         // If an event was raised, the add was successful
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(GameCreated));

         var sut = CreateLeagueActor();

         var createGameCommand = new CreateGame(new LeagueId(Guid.NewGuid()), new SeasonId(SeasonName), new GameNumber(GameNumber));
         sut.Tell(createGameCommand);

         subscriber.ExpectMsg<GameCreated>();
      }

      [Test]
      public void OfficialAddedEventShouldBeRaisedWhenAnOfficialHasBeenAdded()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(OfficialAddedToSeason));
         
         var rating = new Rating(RatingName, Enumerable.Empty<Division>());
         var officialId = new UserId(Guid.NewGuid());

         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonId = new SeasonId(SeasonName);
         var sut = CreateLeagueActor(leagueId);
         sut.Tell(new AddDivisionToRating(leagueId.Value(), seasonId.Value(), DivisionName, RatingName));
         sut.Tell(new AddRatingToSeason(leagueId.Value(), seasonId.Value(), rating.Value()));

         var addOfficialCommand = new AddOfficialToSeason(leagueId.Value(), seasonId.Value(), officialId.Value(), rating.Name);

         sut.Tell(addOfficialCommand);

         subscriber.ExpectMsg<OfficialAddedToSeason>();
      }

      [Test]
      public void AddingADivisionShouldEmitDivisionAddedEvent()
      {
         var division = new Division(DivisionName);
         var seasonId = new SeasonId(SeasonName);

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof (DivisionAddedToSeason));

         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);

         var createDivisionCommand = new AddDivisionToSeason(leagueId.Value(), seasonId.Value(), division);
         sut.Tell(createDivisionCommand);

         subscriber.ExpectMsg<DivisionAddedToSeason>();
      }

      [Test]
      public void AddingARatingShouldEmitRatingAddedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(RatingAddedToSeason));

         var rating = new Rating(RatingName, Enumerable.Empty<Division>());

         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonid = new SeasonId(SeasonName);

         var sut = CreateLeagueActor();

         sut.Tell(new AddRatingToSeason(leagueId.Value(), seasonid.Value(), rating.Value()));
         
         subscriber.ExpectMsg<RatingAddedToSeason>();
      }

      [Test]
      public void AddingADivisionToARatingShouldEmitDivisionAddedToRatingEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(DivisionAddedToRating));
         
         var leagueId = new LeagueId(Guid.NewGuid());
         var seasonId = new SeasonId(SeasonName);
         var sut = CreateLeagueActor();
         
         var rating = new Rating(RatingName, Enumerable.Empty<Division>());
         
         sut.Tell(new AddRatingToSeason(leagueId.Value(), seasonId.Value(), rating.Value()));
         sut.Tell(new AddDivisionToRating(leagueId.Value(), seasonId.Value(), DivisionName, RatingName));

         subscriber.ExpectMsg<DivisionAddedToRating>();
      }

      [Test]
      public async Task AddingASeasonToALeagueShouldBeReportedWhenSeasonsAreQueried()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);

         var seasonId = new SeasonId(SeasonName);

         sut.Tell(new CreateSeason(leagueId, seasonId));

         var answer = await sut.Ask<SeasonsDto>(new ReportSeasons(leagueId.Value()));

         answer.Seasons.Should().Contain(SeasonName);
      }

      [Test]
      public void AddingASeasonShouldEmitSeasonAddedEvent()
      {
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof (SeasonCreated));

         var leagueId = new LeagueId(Guid.NewGuid());
         var sut = CreateLeagueActor(leagueId);

         var seasonId = new SeasonId(SeasonName);

         sut.Tell(new CreateSeason(leagueId, seasonId));

         subscriber.ExpectMsg<SeasonCreated>();
      }

      [Test]
      public void UpdatingAGameSeasonShouldEmitGameSeasonUpdatedEvent()
      {
         const string newSeasonName = "Season2";

         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);
         var seasonId = new SeasonId(SeasonName);
         var newSeasonId = new SeasonId(newSeasonName);

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(GameSeasonUpdated));

         var sut = CreateLeagueActor(leagueId);
         sut.Tell(new UpdateGameSeason(leagueId.Value(), seasonId.Value(), gameNumber.Value(), newSeasonId.Value()));
         
         subscriber.ExpectMsg<GameSeasonUpdated>();
      }

      [Test]
      public void UpdatingAGameTimeShouldEmitGameTimeUpdatedEvent()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var gameNumber = new GameNumber(GameNumber);

         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(GameTimeUpdated));

         var gameTime = TestDataFactory.CreateGameTime();
         var sut = CreateLeagueActor(leagueId);
         sut.Tell(new UpdateGameTime(leagueId.Value(), new SeasonId(SeasonName), gameNumber.Value(), gameTime.Value()));


         subscriber.ExpectMsg<GameTimeUpdated>();
      }

      [Test]
      public void UpdatingTheHomeTeamShouldEmitHomeTeamUpdatedEvent()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(HomeTeamUpdated));

         var sut = CreateLeagueActor(leagueId);

         var team = new Team("HomeTeam");
         sut.Tell(new UpdateHomeTeam(leagueId.Value(), new SeasonId(SeasonName), new GameNumber(GameNumber), team));

         subscriber.ExpectMsg<HomeTeamUpdated>();
      }

      [Test]
      public void UpdatingTheVisitingTeamShouldEmitVisitingTeamUpdatedEvent()
      {
         var leagueId = new LeagueId(Guid.NewGuid());
         var subscriber = CreateTestProbe();
         Sys.EventStream.Subscribe(subscriber.Ref, typeof(VisitingTeamUpdated));

         var sut = CreateLeagueActor(leagueId);

         var team = new Team("HomeTeam");
         sut.Tell(new UpdateVisitingTeam(leagueId.Value(), new SeasonId(SeasonName), new GameNumber(GameNumber), team));

         subscriber.ExpectMsg<VisitingTeamUpdated>();
      }
   }
}