﻿using Akka.Actor;
using Akka.TestKit.NUnit;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Domain.Actors;

namespace Pabran.Assigner.Domain.Unit
{
   [TestFixture, Category("Unit")]
   public class LeagueManagerFixture : TestKit
   {
      [Test]
      public void CanCreateInstance()
      {
         var sut = Sys.ActorOf(Props.Create<LeagueManagerActor>());

         sut.IsNobody().Should().BeFalse();
      } 
   }
}