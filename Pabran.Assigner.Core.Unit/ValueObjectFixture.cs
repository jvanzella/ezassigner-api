﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core.Unit.Fakes;
using Ploeh.AutoFixture;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public class ValueObjectFixture
   {
      private static readonly Fixture Fixture = new Fixture();

      [Test]
      public void ComparingSameObjectShouldReturnTrue()
      {
         var stringValue = Fixture.Create<string>();
         var intValue = Fixture.Create<int>();
         var dateValue = Fixture.Create<DateTimeOffset>();
         var stringList = Fixture.Create<string[]>();

         var sut = new FakeValueObject
         {
            StringProperty = stringValue,
            IntProperty = intValue,
            DateTimeProperty = dateValue,
            StringList = stringList
         };

         sut.Equals(sut).Should().BeTrue();
      }

      [Test]
      public void ObjectsThatAreDifferentReferencesButSameValuesShouldBeEqual()
      {
         var stringValue = Fixture.Create<string>();
         var intValue = Fixture.Create<int>();
         var dateValue = Fixture.Create<DateTimeOffset>();
         var stringList = Fixture.Create<string[]>();

         var sut = new FakeValueObject
         {
            StringProperty = stringValue,
            IntProperty = intValue,
            DateTimeProperty = dateValue,
            StringList = stringList
         };

         var copy = new FakeValueObject
         {
            StringProperty = stringValue,
            IntProperty = intValue,
            DateTimeProperty = dateValue,
            StringList = stringList
         };

         sut.Equals(copy).Should().BeTrue();
      }

      [Test]
      public void ComparingReferenceFromValueShouldEqualFalse()
      {
         var stringValue = Fixture.Create<string>();
         var intValue = Fixture.Create<int>();
         var dateValue = Fixture.Create<DateTimeOffset>();
         var stringList = Fixture.Create<string[]>();

         var sut = new FakeValueObject
         {
            StringProperty = stringValue,
            IntProperty = intValue,
            DateTimeProperty = dateValue,
            StringList = stringList
         };

         var copy = sut.Value();

         sut.Should().NotBeSameAs(copy);
      }

      [Test]
      public void ComparingSameValuesWithNotEqualShouldReturnFalse()
      {
         var stringValue = Fixture.Create<string>();
         var intValue = Fixture.Create<int>();
         var dateValue = Fixture.Create<DateTimeOffset>();
         var stringList = Fixture.Create<string[]>();

         var sut = new FakeValueObject
         {
            StringProperty = stringValue,
            IntProperty = intValue,
            DateTimeProperty = dateValue,
            StringList = stringList
         };

         var copy = sut.Value();

         (sut != copy).Should().BeFalse();
      }
   }
}