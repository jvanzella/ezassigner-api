﻿// /////////////////////////////////////////////////////////
//  PhoneNumberFixture.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////
using FluentAssertions;
using NUnit.Framework;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public sealed class PhoneNumberFixture
   {
      [Test]
      public void CanCreateInstanceWithPhoneNumber()
      {
         var sut = new PhoneNumber(Faker.Phone.Number());

         sut.Should().NotBeNull();
      }

      [Test]
      public void ToStringShouldReturnPhoneNumber()
      {
         var number = Faker.Phone.Number();

         var sut = new PhoneNumber(number);

         sut.ToString().Should().Be(number);
      }

      [Test]
      public void ValueShouldNotReturnSameInstance()
      {
         var sut = new PhoneNumber(Faker.Phone.Number());

         var result = sut.Value();

         result.Should().NotBeSameAs(sut);
      }
   }
}