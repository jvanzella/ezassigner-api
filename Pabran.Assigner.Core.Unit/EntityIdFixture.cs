﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Pabran.Assigner.Core.Unit.Fakes;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public class EntityIdFixture
   {
      [Test]
      public void CanCreateInstance()
      {
         var sut = new FakeEntityId(Guid.NewGuid());

         sut.Should().NotBeNull();
      }

      [Test]
      public void BaseIdPropertyShouldBeSetOnConstruction()
      {
         var baseId = Guid.NewGuid();

         var sut = new FakeEntityId(baseId);

         sut.Id.Should().Be(baseId);
      }

      [Test]
      public void ToStringShouldReturnAFormattedStringRepresentation()
      {
         var baseId = Guid.NewGuid();

         var sut = new FakeEntityId(baseId);

         sut.ToString().Should().Be(string.Format("FakeEntityId({0})", baseId));
      }
   }
}