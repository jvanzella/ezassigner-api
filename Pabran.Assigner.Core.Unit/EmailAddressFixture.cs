﻿// /////////////////////////////////////////////////////////
//  EmailAddressFixture.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////

using System;
using FluentAssertions;
using NUnit.Framework;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public sealed class EmailAddressFixture
   {
      [Test]
      public void CanCreateInstanceWithValidEmailAddress()
      {
         var sut = new EmailAddress(Faker.Internet.Email());

         sut.Should().NotBeNull();
      }

      [TestCase("InvalidEmailhotmail.com")]
      [TestCase("InvalidEmail@hotmailcom")]
      [TestCase("@hotmail.com")]
      public void InvalidEmailShouldThrowInvalidEmailException(string email)
      {
         // ReSharper disable once ObjectCreationAsStatement
         Action action = () => new EmailAddress(email);

         action.ShouldThrow<InvalidEmailException>().WithMessage($"{email} is an invalid email address.");
      }

      [Test]
      public void ToStringShouldReturnTheEmailAddress()
      {
         var email = Faker.Internet.Email();

         var sut = new EmailAddress(email);

         sut.ToString().Should().Be(email);
      }

      [Test]
      public void ValueShouldReturnNewInstance()
      {
         var sut = new EmailAddress(Faker.Internet.Email());

         var result = sut.Value();

         sut.Should().NotBeSameAs(result);
      }
   }
}