﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pabran.Assigner.Core.Unit.Fakes
{
   public class FakeValueObject : ValueObject<FakeValueObject>
   {
      public string StringProperty { get; set; }
      public int IntProperty { get; set; }
      public DateTimeOffset DateTimeProperty { get; set; }
      public IList<string> StringList { get; set; }

      public override FakeValueObject Value()
      {
         return new FakeValueObject
         {
            StringProperty = StringProperty.Value(),
            IntProperty = IntProperty,
            DateTimeProperty = DateTimeProperty.ToUniversalTime(),
            StringList = StringList.ToList()
         };
      }
   }
}