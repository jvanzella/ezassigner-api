﻿using System;

namespace Pabran.Assigner.Core.Unit.Fakes
{
   public class FakeEntityId : EntityId<Guid, FakeEntityId>
   {
      public FakeEntityId(Guid id) : base(id)
      {
      }

      public override FakeEntityId Value()
      {
         return new FakeEntityId(Id);
      }
   }
}