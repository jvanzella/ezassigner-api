﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public class TimeBlockFixture
   {
      [Test]
      public void CanCreateInstance()
      {
         var sut = new GameTime(new DateTimeOffset(DateTime.UtcNow), new DateTimeOffset(DateTime.UtcNow.AddHours(1)));

         sut.Should().NotBeNull();
      }

      [Test]
      public void LengthShouldBeDifferenceBetweenStartTimeAndEndTime()
      {
         var duration = TimeSpan.FromMinutes(90);

         var sut = new GameTime(new DateTimeOffset(DateTime.UtcNow), new DateTimeOffset(DateTime.UtcNow.Add(duration)));

         sut.Duration.Should().Be(duration);
      }
   }
}