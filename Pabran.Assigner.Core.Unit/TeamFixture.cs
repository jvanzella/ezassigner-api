﻿// /////////////////////////////////////////////////////////
//  TeamFixture.cs
// 
//  Created on:      11/25/2016
//  Created by:      Vanzella, Jeffrey (Jeff)
//  Copyright 2016 - Pabran Software
// /////////////////////////////////////////////////////////
using FluentAssertions;
using NUnit.Framework;

namespace Pabran.Assigner.Core.Unit
{
   [TestFixture, Category("Unit")]
   public sealed class TeamFixture
   {
      [Test]
      public void CanCreateInstance()
      {
         var sut = new Team(string.Empty);

         sut.Should().NotBeNull();
      }

      [Test]
      public void NameShouldBeReturnedThroughToString()
      {
         var teamName = Faker.Name.First();
         var sut = new Team(teamName);

         sut.ToString().Should().Be(teamName);
      }

      [Test]
      public void ValueShouldReturnNewInstance()
      {
         var sut = new Team(Faker.Name.First());

         var result = sut.Value();

         result.Should().NotBeSameAs(sut);
      }
   }
}